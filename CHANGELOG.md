# Change Log
All notable changes to this project will be documented in this file.

## 0.1.0 - 2014-12-15
### Added
- Changelog

### Changed
- Fixed dead link in README.md file pointing at OAuth2 website.
 
### Removed
- No change.