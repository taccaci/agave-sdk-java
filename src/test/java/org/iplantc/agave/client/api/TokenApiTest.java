package org.iplantc.agave.client.api;

import org.apache.commons.lang3.StringUtils;
import org.iplantc.agave.client.ApiException;
import org.iplantc.agave.client.JsonUtil;
import org.iplantc.agave.client.Settings;
import org.iplantc.agave.client.model.AccessToken;
import org.iplantc.agave.client.model.TokenResponse;
import org.joda.time.DateTime;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;

public class TokenApiTest {
	
	@BeforeClass
	public void beforeClass() throws ApiException {

	}

	@Test
	public void createAuthToken() {
		try {
			TokenApi api = new TokenApi();
			TokenResponse response = api.createAuthToken(Settings.API_USERNAME, Settings.API_PASSWORD, Settings.API_CLIENT_KEY, Settings.API_CLIENT_SECRET);
			Assert.assertNotNull(response, "Null response from API");
			AccessToken token = response.getResult();
			Assert.assertNotNull(token, "Null token from API");
			Assert.assertFalse(StringUtils.isEmpty(token.getAccessToken()), "Null access token string from API");
			Assert.assertFalse(StringUtils.isEmpty(token.getRefreshToken()), "Null refresh token string from API");
			Assert.assertTrue(token.getCreatedAt().isBeforeNow(), "Token creation time was not in the past.");
			Assert.assertTrue(token.getExpiresAt().isAfterNow(), "Token creation time was not in the past.");
		} catch (ApiException e) {
			Assert.fail("Failed to pull access token from api", e);
		}
	}

	@Test(dependsOnMethods={"createAuthToken"})
	public void refreshAuthToken() 
	{
		try {
			TokenApi api = new TokenApi();
			TokenResponse response = api.createAuthToken(Settings.API_USERNAME, Settings.API_PASSWORD, Settings.API_CLIENT_KEY, Settings.API_CLIENT_SECRET);
			Assert.assertNotNull(response, "Null response from API");
			AccessToken token = response.getResult();
			Assert.assertNotNull(token, "Null token from API");
			Assert.assertFalse(StringUtils.isEmpty(token.getAccessToken()), "Null access token string from API");
			Assert.assertFalse(StringUtils.isEmpty(token.getRefreshToken()), "Null refresh token string from API");
			Assert.assertTrue(token.getCreatedAt().isBeforeNow(), "Token creation time was not in the past.");
			Assert.assertTrue(token.getExpiresAt().isAfterNow(), "Token creation time was not in the past.");
			
			TokenResponse refreshResponse = api.refreshAuthToken(Settings.API_CLIENT_KEY, Settings.API_CLIENT_SECRET, token.getRefreshToken());
			AccessToken refreshToken = refreshResponse.getResult();
			Assert.assertNotNull(refreshToken, "Null refresh token from API");
			Assert.assertFalse(StringUtils.isEmpty(refreshToken.getAccessToken()), "Null refresh access token string from API");
			Assert.assertFalse(StringUtils.isEmpty(refreshToken.getRefreshToken()), "Null refresh refresh token string from API");
			Assert.assertTrue(refreshToken.getCreatedAt().isBefore(new DateTime().minusSeconds(2)), "Refresh token creation time was not in the past.");
			Assert.assertTrue(refreshToken.getExpiresAt().isAfterNow(), "Refresh token expiration time was not in the future.");
			Assert.assertTrue(refreshToken.getExpiresAt().isAfter(token.getExpiresAt()), "Refresh token creation time was after the original token expiration time.");
		} catch (ApiException e) {
			Assert.fail("Failed to pull access token from api", e);
		}
	}
	
	@Test(dependsOnMethods={"refreshAuthToken"})
	public void exportCache() 
	{
		try 
		{
			TokenApi api = new TokenApi();
			TokenResponse response = api.createAuthToken(Settings.API_USERNAME, Settings.API_PASSWORD, Settings.API_CLIENT_KEY, Settings.API_CLIENT_SECRET);
			Assert.assertNotNull(response, "Null response from API");
			AccessToken token = response.getResult();
			Assert.assertNotNull(token, "Null token from API");
			Assert.assertFalse(StringUtils.isEmpty(token.getAccessToken()), "Null access token string from API");
			Assert.assertFalse(StringUtils.isEmpty(token.getRefreshToken()), "Null refresh token string from API");
			Assert.assertTrue(token.getCreatedAt().isBeforeNow(), "Token creation time was not in the past.");
			Assert.assertTrue(token.getExpiresAt().isAfterNow(), "Token creation time was not in the past.");
		
			token.exportCache(Settings.AUTH_CACHE_FILE);
			Assert.assertTrue(Settings.AUTH_CACHE_FILE.exists(), "Cache file does not exist.");
			
			JsonNode json = JsonUtil.getJsonMapper().readTree(Settings.AUTH_CACHE_FILE);
			
			Assert.assertEquals(json.get("access_token").asText(), token.getAccessToken(), 
					"Access token saved does not match that returned from the API");
			
		} catch (Exception e) {
			Assert.fail("Failed to pull access token from api", e);
		}
		finally {
			try { Settings.AUTH_CACHE_FILE.delete(); } catch (Exception e) {}
		}
	}		
	
	@Test(dependsOnMethods={"exportCache"})
	public void importCache() 
	{
		try 
		{
			TokenApi api = new TokenApi();
			TokenResponse response = api.createAuthToken(Settings.API_USERNAME, Settings.API_PASSWORD, Settings.API_CLIENT_KEY, Settings.API_CLIENT_SECRET);
			Assert.assertNotNull(response, "Null response from API");
			AccessToken token = response.getResult();
			Assert.assertNotNull(token, "Null token from API");
			Assert.assertFalse(StringUtils.isEmpty(token.getAccessToken()), "Null access token string from API");
			Assert.assertFalse(StringUtils.isEmpty(token.getRefreshToken()), "Null refresh token string from API");
			Assert.assertTrue(token.getCreatedAt().isBeforeNow(), "Token creation time was not in the past.");
			Assert.assertTrue(token.getExpiresAt().isAfterNow(), "Token creation time was not in the past.");
		
			token.exportCache(Settings.AUTH_CACHE_FILE);
			Assert.assertTrue(Settings.AUTH_CACHE_FILE.exists(), "Cache file does not exist.");
			
			JsonNode json = JsonUtil.getJsonMapper().readTree(Settings.AUTH_CACHE_FILE);
			Assert.assertEquals(json.get("access_token").asText(), token.getAccessToken(), 
					"Access token saved does not match that returned from the API");
			
			AccessToken importToken = AccessToken.importCache(Settings.AUTH_CACHE_FILE);
			Assert.assertTrue(importToken.equals(token), 
					"Access token read does not match that returned from the API and saved");
			
			
		} catch (Exception e) {
			Assert.fail("Failed to pull access token from api", e);
		}
		finally {
			try { Settings.AUTH_CACHE_FILE.delete(); } catch (Exception e) {}
		}
	}
}
