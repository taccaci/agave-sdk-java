/**
 * 
 */
package org.iplantc.agave.client.api;

import org.apache.commons.lang3.StringUtils;
import org.iplantc.agave.client.Settings;
import org.iplantc.agave.client.model.AccessToken;
import org.iplantc.agave.client.model.TokenResponse;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
 * @author dooley
 *
 */
public class AbstractApiTest 
{
	protected AccessToken token;
	
	@BeforeClass
	public void beforeClass() throws Exception
	{
		if (Settings.AUTH_CACHE_FILE.exists()) {
			token = AccessToken.importCache(Settings.AUTH_CACHE_FILE);
		} else {
			TokenApi api = new TokenApi();
			TokenResponse response = api.createAuthToken(Settings.API_USERNAME, Settings.API_PASSWORD, Settings.API_CLIENT_KEY, Settings.API_CLIENT_SECRET);
			token = response.getResult();
			Assert.assertNotNull(token, "Null token from API");
			Assert.assertFalse(StringUtils.isEmpty(token.getAccessToken()), "Null access token string from API");
			Assert.assertFalse(StringUtils.isEmpty(token.getRefreshToken()), "Null refresh token string from API");
			Assert.assertTrue(token.getCreatedAt().isBeforeNow(), "Token creation time was not in the past.");
			Assert.assertTrue(token.getExpiresAt().isAfterNow(), "Token creation time was not in the past.");
			token.exportCache(Settings.AUTH_CACHE_FILE);
		}
	}
	
	@AfterClass
	public void afterClass() throws Exception
	{
		try { Settings.AUTH_CACHE_FILE.delete(); } catch (Exception e) {}
	}
}
	
