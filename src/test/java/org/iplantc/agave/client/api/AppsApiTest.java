package org.iplantc.agave.client.api;

import java.util.List;

import org.iplantc.agave.client.ApiException;
import org.iplantc.agave.client.model.Application;
import org.iplantc.agave.client.model.ApplicationSummary;
import org.iplantc.agave.client.model.MultipleApplicationResponse;
import org.iplantc.agave.client.model.SingleApplicationResponse;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AppsApiTest extends AbstractApiTest
{
	@BeforeClass
	public void beforeClass() throws Exception
	{
		super.beforeClass();
	}
	
	@Test
	public void list() {
		try {
			AppsApi api = new AppsApi(token);
			MultipleApplicationResponse response = api.list(false, false);
			List<ApplicationSummary> apps = response.getResult();
			Assert.assertFalse(apps.isEmpty(), "No apps returned from public and private query");
		} catch (ApiException e) {
			Assert.fail("Failed to query all apps", e);
		}
	}
	
	@Test(dependsOnMethods={"list"})
	public void get() {
		try {
			AppsApi api = new AppsApi(token);
			MultipleApplicationResponse response = api.list(false, false);
			List<ApplicationSummary> apps = response.getResult();
			Assert.assertFalse(apps.isEmpty(), "No apps returned from public and private query");
			
			ApplicationSummary appSummary = apps.get(0);
			
			SingleApplicationResponse appResponse = api.get(appSummary.getId());
			
			Application app = appResponse.getResult();
			Assert.assertNotNull(app, "Named app not returned from api");
			
		} catch (ApiException e) {
			Assert.fail("Failed to query all apps", e);
		}
	}

//	@Test
//	public void add() 
//	{
//		throw new RuntimeException("Test not implemented");
//	}
//
//	@Test
//	public void delete() {
//		throw new RuntimeException("Test not implemented");
//	}
//
//	@Test
//	public void deletePermissions() {
//		throw new RuntimeException("Test not implemented");
//	}
//
//	@Test
//	public void deletePermissionsForUser() {
//		throw new RuntimeException("Test not implemented");
//	}
//
	
//	@Test
//	public void getJobSubmissionForm() {
//		throw new RuntimeException("Test not implemented");
//	}
//
//	@Test
//	public void listByName() {
//		throw new RuntimeException("Test not implemented");
//	}
//
//	@Test
//	public void listByOntologyTerm() {
//		throw new RuntimeException("Test not implemented");
//	}
//
//	@Test
//	public void listBySystemId() {
//		throw new RuntimeException("Test not implemented");
//	}
//
//	@Test
//	public void listByTag() {
//		throw new RuntimeException("Test not implemented");
//	}
//
//	@Test
//	public void listPermissions() {
//		throw new RuntimeException("Test not implemented");
//	}
//
//	@Test
//	public void listPermissionsForUser() {
//		throw new RuntimeException("Test not implemented");
//	}
//
//	@Test
//	public void manage() {
//		throw new RuntimeException("Test not implemented");
//	}
//
//	@Test
//	public void setBasePath() {
//		throw new RuntimeException("Test not implemented");
//	}
//
//	@Test
//	public void update() {
//		throw new RuntimeException("Test not implemented");
//	}
//
//	@Test
//	public void updateApplicationPermissions() {
//		throw new RuntimeException("Test not implemented");
//	}
//
//	@Test
//	public void updatePermissionsForUser() {
//		throw new RuntimeException("Test not implemented");
//	}
}
