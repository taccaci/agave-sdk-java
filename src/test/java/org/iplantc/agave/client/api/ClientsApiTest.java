package org.iplantc.agave.client.api;

import java.util.List;

import org.iplantc.agave.client.ApiException;
import org.iplantc.agave.client.Settings;
import org.iplantc.agave.client.model.Client;
import org.iplantc.agave.client.model.ClientRequest;
import org.iplantc.agave.client.model.MultipleClientResponse;
import org.iplantc.agave.client.model.MultipleSubscriptionResponse;
import org.iplantc.agave.client.model.SingleClientResponse;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ClientsApiTest extends AbstractApiTest 
{	
	@BeforeClass
	public void beforeClass() throws ApiException
	{
		
	}

	@Test
	public void list() {
		try {
			ClientsApi api = new ClientsApi();
			MultipleClientResponse response = api.list(Settings.API_USERNAME, Settings.API_PASSWORD);
			List<Client> clients = response.getResult();
			Assert.assertFalse(clients.isEmpty(), "No clients returned from client service listing");
		} catch (ApiException e) {
			Assert.fail("Failed to query all apps", e);
		}
	}
	
	@Test
	public void getClientByName() {
		try {
			ClientsApi api = new ClientsApi();
			MultipleClientResponse response = api.list(Settings.API_USERNAME, Settings.API_PASSWORD);
			List<Client> clients = response.getResult();
			Assert.assertFalse(clients.isEmpty(), "No clients returned from client service listing");
			
			SingleClientResponse clientResponse = api.getClientByName(Settings.API_USERNAME, Settings.API_PASSWORD, clients.get(0).getName());
			Assert.assertNotNull(clientResponse.getResult(), "Null client description returned for client " + clients.get(0).getName());
		} catch (ApiException e) {
			Assert.fail("Failed to query all apps", e);
		}
	}

	@Test
	public void listSubscriptionsForClient() {
		try {
			ClientsApi api = new ClientsApi();
			MultipleClientResponse response = api.list(Settings.API_USERNAME, Settings.API_PASSWORD);
			List<Client> clients = response.getResult();
			Assert.assertFalse(clients.isEmpty(), "No clients returned from client service listing");
			
			MultipleSubscriptionResponse subscriptionResponse = api.listSubscriptionsForClient(Settings.API_USERNAME, Settings.API_PASSWORD, clients.get(0).getName());
			Assert.assertNotNull(subscriptionResponse.getResult(), "Null subscriptions returned for client " + clients.get(0).getName());
			Assert.assertFalse(subscriptionResponse.getResult().isEmpty(), "No subscriptions returned for client " + clients.get(0).getName());
		} catch (ApiException e) {
			Assert.fail("Failed to query all apps", e);
		}
	}

//	@Test
//	public void addSubscriptionForClient() {
//		throw new RuntimeException("Test not implemented");
//	}
//
//	@Test
//	public void deleteSubscriptionsForClient() {
//		throw new RuntimeException("Test not implemented");
//	}

	@Test
	public void create() {
		try {
			ClientsApi api = new ClientsApi();
			ClientRequest request = new ClientRequest();
			request.setName("my-test-client");
			request.setDescription("test client created for unit tests");
			request.setTier("UNLIMITED");
			request.setCallbackUrl(null);
			
			SingleClientResponse response = api.create(Settings.API_USERNAME, Settings.API_PASSWORD, request);
			Client client = response.getResult();
			Assert.assertNotNull(client, "No clients returned after creation");
			
			SingleClientResponse clientResponse = api.getClientByName(Settings.API_USERNAME, Settings.API_PASSWORD, request.getName());
			Assert.assertNotNull(clientResponse.getResult(), "Null client description returned for client " + request.getName());
			
		} catch (ApiException e) {
			Assert.fail("Failed to query all apps", e);
		}
	}
	
	@Test
	public void delete() {
		try {
			ClientsApi api = new ClientsApi();
			ClientRequest request = new ClientRequest();
			request.setName("my-test-client");
			request.setDescription("test client created for unit tests");
			request.setTier("UNLIMITED");
			request.setCallbackUrl(null);
			
			SingleClientResponse response = api.create(Settings.API_USERNAME, Settings.API_PASSWORD, request);
			Client client = response.getResult();
			Assert.assertNotNull(client, "No clients returned after creation");
			
			SingleClientResponse clientResponse = api.getClientByName(Settings.API_USERNAME, Settings.API_PASSWORD, request.getName());
			Assert.assertNotNull(clientResponse.getResult(), "Null client description returned for client " + request.getName());
			
			api.delete(Settings.API_USERNAME, Settings.API_PASSWORD, request.getName());
			
			clientResponse = api.getClientByName(Settings.API_USERNAME, Settings.API_PASSWORD, request.getName());
			Assert.assertNull(clientResponse.getResult(), "Client description was returned for deleted client " + request.getName());
					
		} catch (ApiException e) {
			Assert.fail("Failed to query all apps", e);
		}
	}
}
