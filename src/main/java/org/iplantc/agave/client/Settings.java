package org.iplantc.agave.client;

import java.io.File;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

public class Settings {

	private static Properties 		props = new Properties();
	
	public static String			API_BASE_URL;
	public static String			API_VERSION;
	public static String 			API_USERNAME;
    public static String 			API_PASSWORD;

    public static String			API_CLIENT_KEY;
    public static String			API_CLIENT_SECRET;
    public static String			API_ACCESS_TOKEN;
    
    public static File				AUTH_CACHE_FILE;
    
    
	static 
	{
		try
		{
			props.load(Settings.class.getClassLoader().getResourceAsStream("client.properties"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		API_BASE_URL = (String) props.getProperty("api.base.url", "https://agave.iplantc.org");
		while (StringUtils.endsWith(API_BASE_URL, "/")) {
			API_BASE_URL = StringUtils.substring(API_BASE_URL, 0, -1);
		}
		
		API_VERSION = (String) props.getProperty("api.version", "v2");
		
		String authCachFilePath = props.getProperty("api.auth.cache.file", System.getProperty("user.home") + File.separator + ".agave");
		AUTH_CACHE_FILE = new File(authCachFilePath);
		
		API_ACCESS_TOKEN = (String) props.getProperty("api.access.token");

		API_CLIENT_KEY = (String) props.get("api.client.key");
		API_CLIENT_SECRET = (String) props.get("api.client.secret");
		
		API_USERNAME = (String) props.get("api.username");
		API_PASSWORD = (String) props.get("api.password");
	}
}
