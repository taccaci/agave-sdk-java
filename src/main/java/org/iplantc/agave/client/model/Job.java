package org.iplantc.agave.client.model;

import java.util.Date;
import java.util.*;
import org.iplantc.agave.client.model.JobParameters;
import org.iplantc.agave.client.model.JobInputs;
import org.iplantc.agave.client.model.Notification;

public class Job {
	/*
	 * The unique name of the application being run by this job. This must be a
	 * valid application that the calling user has permission to run.
	 */
	private String appId = null;
	/*
	 * Whether the output from this job should be archived. If true, all new
	 * files created by this application's execution will be archived to the
	 * archivePath in the user's default storage system.
	 */
	private Boolean archive = null;
	/*
	 * The path of the archive folder for this job on the user's default storage
	 * sytem.
	 */
	private String archivePath = null;
	/*
	 * The unique id of the storage system on which this job's output will be
	 * staged.
	 */
	private String archiveSystem = null;
	/*
	 * The queue to which this job should be submitted. This is optional and
	 * only applies when the execution system has a batch scheduler.
	 */
	private String batchQueue = null;
	/*
	 * The date the job stopped running due to termination, completion, or error
	 * in ISO 8601 format.
	 */
	private Date endTime = null;
	/* The system id of the execution system. */
	private String executionSystem = null;
	/* The unique id of the job. */
	private Integer id = null;
	/*
	 * The application specific input files needed for this job. These vary from
	 * application to application and should be entered as multiple individual
	 * parameters in the form. Inputs may be given as relative paths in the
	 * user's default storage system or as URI. If a URI is given, the data will
	 * be staged in by the IO service and made avaialble to the application at
	 * run time.
	 */
	private JobInputs inputs = null;
	/* The process or local job id of the job on the remote execution system. */
	private String localId = null;
	/* The requested memory for this application to run given in GB. */
	private String memoryPerNode = null;
	/* The error message incurred when the job failed. */
	private String message = null;
	/* The name of the job. */
	private String name = null;
	/*
	 * The number of processors this application should utilize while running.
	 * If the application is not of executionType PARALLEL, this should be 1.
	 */
	private Integer nodeCount = null;
	/* An array of notifications you wish to receive. */
	private List<Notification> notifications = new ArrayList<Notification>();
	/* Relative path of the job's output data. */
	private String outputPath = null;
	/* The job owner. */
	private String owner = null;
	/*
	 * The application specific parameters needed for this job. These vary from
	 * application to application and should be entered as multiple individual
	 * parameters in the form. The actual dataType will be determined by the
	 * application description.
	 */
	private JobParameters parameters = null;
	/*
	 * The number of processors this application should utilize while running.
	 * If the application is not of executionType PARALLEL, this should be 1.
	 */
	private Integer processorsPerNode = null;
	/*
	 * The requested compute time needed for this application to complete given
	 * in HH:mm:ss format.
	 */
	private String maxRunTime = null;
	/* The number of retires it took to submit this job. */
	private Integer retries = null;
	/* The date the job started in ISO 8601 format. */
	private Date startTime = null;
	/*
	 * The status of the job. Possible values are: PENDING, STAGING_INPUTS,
	 * CLEANING_UP, ARCHIVING, STAGING_JOB, FINISHED, KILLED, FAILED, STOPPED,
	 * RUNNING, PAUSED, QUEUED, SUBMITTING, STAGED, PROCESSING_INPUTS,
	 * ARCHIVING_FINISHED, ARCHIVING_FAILED
	 */
	private String status = null;
	/* The date the job was submitted in ISO 8601 format. */
	private Date submitTime = null;
	/*
	 * The directory on the remote execution system from which the job is
	 * running.
	 */
	private String workPath = null;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public Boolean getArchive() {
		return archive;
	}

	public void setArchive(Boolean archive) {
		this.archive = archive;
	}

	public String getArchivePath() {
		return archivePath;
	}

	public void setArchivePath(String archivePath) {
		this.archivePath = archivePath;
	}

	public String getArchiveSystem() {
		return archiveSystem;
	}

	public void setArchiveSystem(String archiveSystem) {
		this.archiveSystem = archiveSystem;
	}

	public String getBatchQueue() {
		return batchQueue;
	}

	public void setBatchQueue(String batchQueue) {
		this.batchQueue = batchQueue;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getExecutionSystem() {
		return executionSystem;
	}

	public void setExecutionSystem(String executionSystem) {
		this.executionSystem = executionSystem;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public JobInputs getInputs() {
		return inputs;
	}

	public void setInputs(JobInputs inputs) {
		this.inputs = inputs;
	}

	public String getLocalId() {
		return localId;
	}

	public void setLocalId(String localId) {
		this.localId = localId;
	}

	public String getMemoryPerNode() {
		return memoryPerNode;
	}

	public void setMemoryPerNode(String memoryPerNode) {
		this.memoryPerNode = memoryPerNode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNodeCount() {
		return nodeCount;
	}

	public void setNodeCount(Integer nodeCount) {
		this.nodeCount = nodeCount;
	}

	public List<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	public String getOutputPath() {
		return outputPath;
	}

	public void setOutputPath(String outputPath) {
		this.outputPath = outputPath;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public JobParameters getParameters() {
		return parameters;
	}

	public void setParameters(JobParameters parameters) {
		this.parameters = parameters;
	}

	public Integer getProcessorsPerNode() {
		return processorsPerNode;
	}

	public void setProcessorsPerNode(Integer processorsPerNode) {
		this.processorsPerNode = processorsPerNode;
	}

	public String getMaxRunTime() {
		return maxRunTime;
	}

	public void setMaxRunTime(String maxRunTime) {
		this.maxRunTime = maxRunTime;
	}

	public Integer getRetries() {
		return retries;
	}

	public void setRetries(Integer retries) {
		this.retries = retries;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(Date submitTime) {
		this.submitTime = submitTime;
	}

	public String getWorkPath() {
		return workPath;
	}

	public void setWorkPath(String workPath) {
		this.workPath = workPath;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Job {\n");
		sb.append("  appId: ").append(appId).append("\n");
		sb.append("  archive: ").append(archive).append("\n");
		sb.append("  archivePath: ").append(archivePath).append("\n");
		sb.append("  archiveSystem: ").append(archiveSystem).append("\n");
		sb.append("  batchQueue: ").append(batchQueue).append("\n");
		sb.append("  endTime: ").append(endTime).append("\n");
		sb.append("  executionSystem: ").append(executionSystem).append("\n");
		sb.append("  id: ").append(id).append("\n");
		sb.append("  inputs: ").append(inputs).append("\n");
		sb.append("  localId: ").append(localId).append("\n");
		sb.append("  memoryPerNode: ").append(memoryPerNode).append("\n");
		sb.append("  message: ").append(message).append("\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("  nodeCount: ").append(nodeCount).append("\n");
		sb.append("  notifications: ").append(notifications).append("\n");
		sb.append("  outputPath: ").append(outputPath).append("\n");
		sb.append("  owner: ").append(owner).append("\n");
		sb.append("  parameters: ").append(parameters).append("\n");
		sb.append("  processorsPerNode: ").append(processorsPerNode)
				.append("\n");
		sb.append("  maxRunTime: ").append(maxRunTime).append("\n");
		sb.append("  retries: ").append(retries).append("\n");
		sb.append("  startTime: ").append(startTime).append("\n");
		sb.append("  status: ").append(status).append("\n");
		sb.append("  submitTime: ").append(submitTime).append("\n");
		sb.append("  workPath: ").append(workPath).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
