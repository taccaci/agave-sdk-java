package org.iplantc.agave.client.model;

import org.iplantc.agave.client.model.UserCredentialServer;

public class UserCredential {
	/*
	 * The credential used to authenticate to the remote system. Depending on
	 * the authentication protocol of the remote system, this could be an OAuth
	 * Token, X.509 certificate, Kerberose token, or an private key..
	 */
	private String credential = null;
	/* The username of the internal user associated with this credential. */
	private String internalUsername = null;
	/* The password on the remote system used to authenticate. */
	private String password = null;
	/* The public ssh key used to authenticate to the remote system. */
	private String publicKey = null;
	/* The public ssh key used to authenticate to the remote system.. */
	private String privateKey = null;
	/* The server from which a credential may be obtained. */
	private UserCredentialServer server = null;
	/* The authentication type. */
	private String type = null;
	// public enum typeEnum { LOCAL, PAM, PASSWORD, SSHKEYS, TOKEN, X509, };
	/* The local username on the remote system used to authenticate. */
	private String username = null;

	public String getCredential() {
		return credential;
	}

	public void setCredential(String credential) {
		this.credential = credential;
	}

	public String getInternalUsername() {
		return internalUsername;
	}

	public void setInternalUsername(String internalUsername) {
		this.internalUsername = internalUsername;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public UserCredentialServer getServer() {
		return server;
	}

	public void setServer(UserCredentialServer server) {
		this.server = server;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class UserCredential {\n");
		sb.append("  credential: ").append(credential).append("\n");
		sb.append("  internalUsername: ").append(internalUsername).append("\n");
		sb.append("  password: ").append(password).append("\n");
		sb.append("  publicKey: ").append(publicKey).append("\n");
		sb.append("  privateKey: ").append(privateKey).append("\n");
		sb.append("  server: ").append(server).append("\n");
		sb.append("  type: ").append(type).append("\n");
		sb.append("  username: ").append(username).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
