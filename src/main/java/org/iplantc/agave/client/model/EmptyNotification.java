package org.iplantc.agave.client.model;

public class EmptyNotification {
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class EmptyNotification {\n");
		sb.append("}\n");
		return sb.toString();
	}
}
