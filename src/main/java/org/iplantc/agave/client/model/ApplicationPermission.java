package org.iplantc.agave.client.model;

import org.iplantc.agave.client.model.ACL;

public class ApplicationPermission {
	private ACL permission = null;
	/* Username associate with this permission */
	private String username = null;

	public ACL getPermission() {
		return permission;
	}

	public void setPermission(ACL permission) {
		this.permission = permission;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ApplicationPermission {\n");
		sb.append("  permission: ").append(permission).append("\n");
		sb.append("  username: ").append(username).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
