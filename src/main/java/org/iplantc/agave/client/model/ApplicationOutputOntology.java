package org.iplantc.agave.client.model;

import java.util.*;

public class ApplicationOutputOntology {
	/* The file types acceptable for this output. */
	private List<String> fileTypes = new ArrayList<String>();
	/* The maximum number of times this output may appear. */
	private Integer maxCardinality = null;
	/* The minimum number of times this output may appear. */
	private Integer minCardinality = null;
	private List<String> ontology = new ArrayList<String>();

	public List<String> getFileTypes() {
		return fileTypes;
	}

	public void setFileTypes(List<String> fileTypes) {
		this.fileTypes = fileTypes;
	}

	public Integer getMaxCardinality() {
		return maxCardinality;
	}

	public void setMaxCardinality(Integer maxCardinality) {
		this.maxCardinality = maxCardinality;
	}

	public Integer getMinCardinality() {
		return minCardinality;
	}

	public void setMinCardinality(Integer minCardinality) {
		this.minCardinality = minCardinality;
	}

	public List<String> getOntology() {
		return ontology;
	}

	public void setOntology(List<String> ontology) {
		this.ontology = ontology;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ApplicationOutputOntology {\n");
		sb.append("  fileTypes: ").append(fileTypes).append("\n");
		sb.append("  maxCardinality: ").append(maxCardinality).append("\n");
		sb.append("  minCardinality: ").append(minCardinality).append("\n");
		sb.append("  ontology: ").append(ontology).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
