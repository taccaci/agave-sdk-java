package org.iplantc.agave.client.model;

public class Subscription {
	/* The base url path of the API. */
	private String apiContext = null;
	/* The name of the API. */
	private String apiName = null;
	/* The user who registered the API. */
	private String apiProvider = null;
	/*
	 * The current major version of the API. This is appended to the api_context
	 * to create the base API url.
	 */
	private String apiVersion = null;
	/* The current status of the API. */
	private String status = null;
	/* The access tier for this client. */
	private String tier = null;

	public String getApiContext() {
		return apiContext;
	}

	public void setApiContext(String apiContext) {
		this.apiContext = apiContext;
	}

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String getApiProvider() {
		return apiProvider;
	}

	public void setApiProvider(String apiProvider) {
		this.apiProvider = apiProvider;
	}

	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTier() {
		return tier;
	}

	public void setTier(String tier) {
		this.tier = tier;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Subscription {\n");
		sb.append("  apiContext: ").append(apiContext).append("\n");
		sb.append("  apiName: ").append(apiName).append("\n");
		sb.append("  apiProvider: ").append(apiProvider).append("\n");
		sb.append("  apiVersion: ").append(apiVersion).append("\n");
		sb.append("  status: ").append(status).append("\n");
		sb.append("  tier: ").append(tier).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
