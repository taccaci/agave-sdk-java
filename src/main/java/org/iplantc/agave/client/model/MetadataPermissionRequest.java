package org.iplantc.agave.client.model;

public class MetadataPermissionRequest {
	/* The username of the api user whose permission is to be set. */
	private String username = null;
	/* The permission to set */
	private String permission = null;

	// public enum permissionEnum { READ, WRITE, READ_WRITE, ALL, NONE, };
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class MetadataPermissionRequest {\n");
		sb.append("  username: ").append(username).append("\n");
		sb.append("  permission: ").append(permission).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
