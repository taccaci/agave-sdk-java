package org.iplantc.agave.client.model;

public class BatchQueue {
	/*
	 * Any custom directives that should be appended to scheduler directives.
	 * ex. #$ -A TG-12345
	 */
	private String customDirectives = null;
	/* Is this the default queue for the system. */
	private Boolean isDefault = null;
	/* The maximum number of jobs that can be in queue at once. */
	private Integer maxJobs = null;
	/* The maximum number of jobs per user that can be in queue at once. */
	private Integer maxUserJobs = null;
	/* The max nodes available per node to jobs submitted to this queue. */
	private String maxNodes = null;
	/* The max memory available per node to jobs submitted to this queue. */
	private String maxMemoryPerNode = null;
	/* The max processors per node available to jobs submitted to this queue. */
	private Integer maxProcessorsPerNode = null;
	/* The max length of jobs submitted to this queue in hhh:mm:ss format. */
	private String maxRequestedTime = null;
	/* The name of the batch queue. */
	private String name = null;

	public String getCustomDirectives() {
		return customDirectives;
	}

	public void setCustomDirectives(String customDirectives) {
		this.customDirectives = customDirectives;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Integer getMaxJobs() {
		return maxJobs;
	}

	public void setMaxJobs(Integer maxJobs) {
		this.maxJobs = maxJobs;
	}

	public Integer getMaxUserJobs() {
		return maxUserJobs;
	}

	public void setMaxUserJobs(Integer maxUserJobs) {
		this.maxUserJobs = maxUserJobs;
	}

	public String getMaxNodes() {
		return maxNodes;
	}

	public void setMaxNodes(String maxNodes) {
		this.maxNodes = maxNodes;
	}

	public String getMaxMemoryPerNode() {
		return maxMemoryPerNode;
	}

	public void setMaxMemoryPerNode(String maxMemoryPerNode) {
		this.maxMemoryPerNode = maxMemoryPerNode;
	}

	public Integer getMaxProcessorsPerNode() {
		return maxProcessorsPerNode;
	}

	public void setMaxProcessorsPerNode(Integer maxProcessorsPerNode) {
		this.maxProcessorsPerNode = maxProcessorsPerNode;
	}

	public String getMaxRequestedTime() {
		return maxRequestedTime;
	}

	public void setMaxRequestedTime(String maxRequestedTime) {
		this.maxRequestedTime = maxRequestedTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class BatchQueue {\n");
		sb.append("  customDirectives: ").append(customDirectives).append("\n");
		sb.append("  isDefault: ").append(isDefault).append("\n");
		sb.append("  maxJobs: ").append(maxJobs).append("\n");
		sb.append("  maxUserJobs: ").append(maxUserJobs).append("\n");
		sb.append("  maxNodes: ").append(maxNodes).append("\n");
		sb.append("  maxMemoryPerNode: ").append(maxMemoryPerNode).append("\n");
		sb.append("  maxProcessorsPerNode: ").append(maxProcessorsPerNode)
				.append("\n");
		sb.append("  maxRequestedTime: ").append(maxRequestedTime).append("\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
