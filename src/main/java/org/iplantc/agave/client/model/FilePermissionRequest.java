package org.iplantc.agave.client.model;

public class FilePermissionRequest {
	/* The username of the api user whose permission is to be set. */
	private String username = null;
	/* The permission to set */
	private String permission = null;
	// public enum permissionEnum { READ, WRITE, EXECUTE, READ_WRITE,
	// READ_EXECUTE, WRITE_EXECUTE, ALL, NONE, };
	/* Should updated permissions be applied recursively. Defaults to false. */
	private Boolean recursive = null;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public Boolean getRecursive() {
		return recursive;
	}

	public void setRecursive(Boolean recursive) {
		this.recursive = recursive;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class FilePermissionRequest {\n");
		sb.append("  username: ").append(username).append("\n");
		sb.append("  permission: ").append(permission).append("\n");
		sb.append("  recursive: ").append(recursive).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
