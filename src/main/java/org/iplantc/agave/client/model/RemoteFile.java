package org.iplantc.agave.client.model;

import java.util.Date;

public class RemoteFile {
	/* The file type of the file. */
	private String format = null;
	/* The date this file was last modified in ISO 8601 format. */
	private Date lastModified = null;
	/* The length of the file/folder. */
	private Integer length = null;
	/*
	 * The mime type of the file/folder. If unknown, it defaults to
	 * application/binary.
	 */
	private String mimeType = null;
	/* The name of the file/folder. */
	private String name = null;
	/* The absolute path to the file/folder. */
	private String path = null;
	/* The system permission of the invoking user on the file/folder. */
	private String permissions = null;
	/* The systemId of the system where this file lives. */
	private String system = null;
	/* Whether it is a file or folder. */
	private String type = null;

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPermissions() {
		return permissions;
	}

	public void setPermissions(String permissions) {
		this.permissions = permissions;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class RemoteFile {\n");
		sb.append("  format: ").append(format).append("\n");
		sb.append("  lastModified: ").append(lastModified).append("\n");
		sb.append("  length: ").append(length).append("\n");
		sb.append("  mimeType: ").append(mimeType).append("\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("  path: ").append(path).append("\n");
		sb.append("  permissions: ").append(permissions).append("\n");
		sb.append("  system: ").append(system).append("\n");
		sb.append("  type: ").append(type).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
