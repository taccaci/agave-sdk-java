package org.iplantc.agave.client.model;

import java.util.Date;

public class ApplicationSummary {
	/* Unique id of this app. Comprised of the app name-version. */
	private String id = null;
	/* The system id of the execution system where this app should run. */
	private String executionSystem = null;
	/* The date this application was last modified in ISO 8601 format. */
	private Date lastModified = null;
	/*
	 * The name of the application. The name does not have to be unique, but the
	 * combination of name and version does.
	 */
	private String name = null;
	/* Whether the application is public or private. */
	private Boolean isPublic = null;
	/* The number of times this application has been revised. */
	private Integer revision = null;
	/* The short description of this application. */
	private String shortDescription = null;
	/*
	 * The version of the application in #.#.# format. While the version does
	 * not need to be unique, the combination of name and version does have to
	 * be unique.
	 */
	private String version = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getExecutionSystem() {
		return executionSystem;
	}

	public void setExecutionSystem(String executionSystem) {
		this.executionSystem = executionSystem;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Boolean isPublic) {
		this.isPublic = isPublic;
	}

	public Integer getRevision() {
		return revision;
	}

	public void setRevision(Integer revision) {
		this.revision = revision;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ApplicationSummary {\n");
		sb.append("  id: ").append(id).append("\n");
		sb.append("  executionSystem: ").append(executionSystem).append("\n");
		sb.append("  lastModified: ").append(lastModified).append("\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("  isPublic: ").append(isPublic).append("\n");
		sb.append("  revision: ").append(revision).append("\n");
		sb.append("  shortDescription: ").append(shortDescription).append("\n");
		sb.append("  version: ").append(version).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
