package org.iplantc.agave.client.model;

import java.util.*;

public class MetadataRequest {
	/* The name of this metadata */
	private String name = null;
	/*
	 * A free text or JSON string containing the metadata stored for the given
	 * associationIds
	 */
	private String value = null;
	/* The UUID of the schema that should be used to validate this request. */
	private String schemaId = null;
	/*
	 * UUIDs of associated Agave entities, including the Data to which this
	 * Metadata belongs.
	 */
	private List<String> associationIds = new ArrayList<String>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getSchemaId() {
		return schemaId;
	}

	public void setSchemaId(String schemaId) {
		this.schemaId = schemaId;
	}

	public List<String> getAssociationIds() {
		return associationIds;
	}

	public void setAssociationIds(List<String> associationIds) {
		this.associationIds = associationIds;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class MetadataRequest {\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("  value: ").append(value).append("\n");
		sb.append("  schemaId: ").append(schemaId).append("\n");
		sb.append("  associationIds: ").append(associationIds).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
