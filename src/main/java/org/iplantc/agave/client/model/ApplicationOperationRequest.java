package org.iplantc.agave.client.model;

public class ApplicationOperationRequest {
	/* Action to perform on the file or folder. */
	private String action = null;
	// public enum actionEnum { publish, clone, };
	/* Name of cloned app. Only used with the clone action. */
	private String name = null;
	/* Version of the cloned app. Only used with the clone action. */
	private String version = null;
	/*
	 * Path to the on cloned app's deployment folder on its storage system. Only
	 * used with the clone action.
	 */
	private String deploymentPath = null;
	/*
	 * Storage system on which the cloned app's assets resides. Only used with
	 * the clone action.
	 */
	private String storageSystem = null;
	/*
	 * System on which the clone apps should run. Only used with the clone
	 * action.
	 */
	private String executionSystem = null;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDeploymentPath() {
		return deploymentPath;
	}

	public void setDeploymentPath(String deploymentPath) {
		this.deploymentPath = deploymentPath;
	}

	public String getStorageSystem() {
		return storageSystem;
	}

	public void setStorageSystem(String storageSystem) {
		this.storageSystem = storageSystem;
	}

	public String getExecutionSystem() {
		return executionSystem;
	}

	public void setExecutionSystem(String executionSystem) {
		this.executionSystem = executionSystem;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ApplicationOperationRequest {\n");
		sb.append("  action: ").append(action).append("\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("  version: ").append(version).append("\n");
		sb.append("  deploymentPath: ").append(deploymentPath).append("\n");
		sb.append("  storageSystem: ").append(storageSystem).append("\n");
		sb.append("  executionSystem: ").append(executionSystem).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
