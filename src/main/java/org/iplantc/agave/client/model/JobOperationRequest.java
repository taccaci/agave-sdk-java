package org.iplantc.agave.client.model;

public class JobOperationRequest {
	/* Action to perform on the job. */
	private String action = null;

	// public enum actionEnum { resubmit, stop, };
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class JobOperationRequest {\n");
		sb.append("  action: ").append(action).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
