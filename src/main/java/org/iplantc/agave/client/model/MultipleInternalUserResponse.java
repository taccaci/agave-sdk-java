package org.iplantc.agave.client.model;

import java.util.*;
import org.iplantc.agave.client.model.InternalUser;

public class MultipleInternalUserResponse {
	/* success or failure */
	private String message = null;
	/* response body */
	private List<InternalUser> result = new ArrayList<InternalUser>();
	/* success or failure */
	private String status = null;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<InternalUser> getResult() {
		return result;
	}

	public void setResult(List<InternalUser> result) {
		this.result = result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class MultipleInternalUserResponse {\n");
		sb.append("  message: ").append(message).append("\n");
		sb.append("  result: ").append(result).append("\n");
		sb.append("  status: ").append(status).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
