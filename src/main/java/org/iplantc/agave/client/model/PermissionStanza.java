package org.iplantc.agave.client.model;

public class PermissionStanza {
	/* Is the file executable */
	private Boolean execute = null;
	/* Is the file readable */
	private Boolean read = null;
	/* Is the file writable */
	private Boolean write = null;

	public Boolean getExecute() {
		return execute;
	}

	public void setExecute(Boolean execute) {
		this.execute = execute;
	}

	public Boolean getRead() {
		return read;
	}

	public void setRead(Boolean read) {
		this.read = read;
	}

	public Boolean getWrite() {
		return write;
	}

	public void setWrite(Boolean write) {
		this.write = write;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class PermissionStanza {\n");
		sb.append("  execute: ").append(execute).append("\n");
		sb.append("  read: ").append(read).append("\n");
		sb.append("  write: ").append(write).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
