package org.iplantc.agave.client.model;

public class EmptyMetadata {
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class EmptyMetadata {\n");
		sb.append("}\n");
		return sb.toString();
	}
}
