package org.iplantc.agave.client.model;

public class TransformDecoder {
	/* Description of this transform decoder. */
	private String description = null;
	private String name = null;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class TransformDecoder {\n");
		sb.append("  description: ").append(description).append("\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
