package org.iplantc.agave.client.model;

import java.util.*;

public class ApplicationParameterOntology {
	private List<String> ontology = new ArrayList<String>();

	public List<String> getOntology() {
		return ontology;
	}

	public void setOntology(List<String> ontology) {
		this.ontology = ontology;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ApplicationParameterOntology {\n");
		sb.append("  ontology: ").append(ontology).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
