package org.iplantc.agave.client.model;

public class ApplicationInputValue {
	/*
	 * The default value for this parameter. The type will be determined by the
	 * value.type field.
	 */
	private String defaultValue = null;
	/*
	 * The order in which this parameter should be printed when generating an
	 * execution command for forked execution. This will also be the order in
	 * which paramters are returned in the response json.
	 */
	private Integer order = null;
	/* Is this parameter required? If visible is false, this must be true. */
	private Boolean required = null;
	/*
	 * The regular expression used to validate this parameter value. For
	 * enumerations, separate values with |
	 */
	private String validator = null;
	/*
	 * Should this parameter be visible? If not, there must be a default and it
	 * will be required.
	 */
	private Boolean visible = null;

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public String getValidator() {
		return validator;
	}

	public void setValidator(String validator) {
		this.validator = validator;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ApplicationInputValue {\n");
		sb.append("  defaultValue: ").append(defaultValue).append("\n");
		sb.append("  order: ").append(order).append("\n");
		sb.append("  required: ").append(required).append("\n");
		sb.append("  validator: ").append(validator).append("\n");
		sb.append("  visible: ").append(visible).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
