package org.iplantc.agave.client.model;

public class EmptyMonitor {
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class EmptyMonitor {\n");
		sb.append("}\n");
		return sb.toString();
	}
}
