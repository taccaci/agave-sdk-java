package org.iplantc.agave.client.model;

public class UserCredentialServer {
	/* The hostname or ip address of the remote server. */
	private String endpoint = null;
	/* The port number of the remote server.. */
	private Integer port = null;
	/* The protocol used to obtain a credential from the remote server. */
	private String protocol = null;

	// public enum protocolEnum { MYPROXY, OA4MP, };
	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class UserCredentialServer {\n");
		sb.append("  endpoint: ").append(endpoint).append("\n");
		sb.append("  port: ").append(port).append("\n");
		sb.append("  protocol: ").append(protocol).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
