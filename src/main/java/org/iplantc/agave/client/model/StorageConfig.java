package org.iplantc.agave.client.model;

import org.iplantc.agave.client.model.UserCredential;

public class StorageConfig {
	/* The default authentication credential used for this system. */
	private UserCredential auth = null;
	/*
	 * The path on the remote system to use as the home directory for all API
	 * requests. For cases where the home directory should be dynamically
	 * defined, the ${USERNAME} token can be included in the homeDir value to
	 * represent the username of the authenticated user. ex. /home/${USERNAME}.
	 */
	private String homeDir = null;
	/* The hostname or ip address of the storage server */
	private String host = null;
	/* The port number of the storage server. */
	private Integer port = null;
	/*
	 * Whether the permissions set on the server should be pushed to the storage
	 * system itself. This only applies to IRODS and AWS systems.
	 */
	private Boolean mirror = null;
	/* The protocol used to authenticate to the storage server. */
	private String protocol = null;
	// public enum protocolEnum { GRIDFTP, FTP, SFTP, IRODS, LOCAL, };
	/*
	 * The path on the remote system where apps will be stored if this is set as
	 * the default public storage system.
	 */
	private String publicAppsDir = null;
	/* The name of the target resource when defining an IRODS system. */
	private String resource = null;
	/* The path on the remote system to use as the root for all API requests. */
	private String rootDir = null;
	/* The name of the zone when defining an IRODS system. */
	private String zone = null;

	public UserCredential getAuth() {
		return auth;
	}

	public void setAuth(UserCredential auth) {
		this.auth = auth;
	}

	public String getHomeDir() {
		return homeDir;
	}

	public void setHomeDir(String homeDir) {
		this.homeDir = homeDir;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public Boolean getMirror() {
		return mirror;
	}

	public void setMirror(Boolean mirror) {
		this.mirror = mirror;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getPublicAppsDir() {
		return publicAppsDir;
	}

	public void setPublicAppsDir(String publicAppsDir) {
		this.publicAppsDir = publicAppsDir;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public String getRootDir() {
		return rootDir;
	}

	public void setRootDir(String rootDir) {
		this.rootDir = rootDir;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class StorageConfig {\n");
		sb.append("  auth: ").append(auth).append("\n");
		sb.append("  homeDir: ").append(homeDir).append("\n");
		sb.append("  host: ").append(host).append("\n");
		sb.append("  port: ").append(port).append("\n");
		sb.append("  mirror: ").append(mirror).append("\n");
		sb.append("  protocol: ").append(protocol).append("\n");
		sb.append("  publicAppsDir: ").append(publicAppsDir).append("\n");
		sb.append("  resource: ").append(resource).append("\n");
		sb.append("  rootDir: ").append(rootDir).append("\n");
		sb.append("  zone: ").append(zone).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
