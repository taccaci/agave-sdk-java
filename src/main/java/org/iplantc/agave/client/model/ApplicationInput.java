package org.iplantc.agave.client.model;

import org.iplantc.agave.client.model.ApplicationInputDetails;
import org.iplantc.agave.client.model.ApplicationInputValue;
import org.iplantc.agave.client.model.ApplicationInputOntology;

public class ApplicationInput {
	/* The details for this input. */
	private ApplicationInputDetails details = null;
	/*
	 * The id of this input. This will be the replacement string in your wrapper
	 * scripts.
	 */
	private String id = null;
	/* The ontologies for this input. */
	private ApplicationInputOntology semantics = null;
	/* The inputs files for this input. */
	private ApplicationInputValue value = null;

	public ApplicationInputDetails getDetails() {
		return details;
	}

	public void setDetails(ApplicationInputDetails details) {
		this.details = details;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ApplicationInputOntology getSemantics() {
		return semantics;
	}

	public void setSemantics(ApplicationInputOntology semantics) {
		this.semantics = semantics;
	}

	public ApplicationInputValue getValue() {
		return value;
	}

	public void setValue(ApplicationInputValue value) {
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ApplicationInput {\n");
		sb.append("  details: ").append(details).append("\n");
		sb.append("  id: ").append(id).append("\n");
		sb.append("  semantics: ").append(semantics).append("\n");
		sb.append("  value: ").append(value).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
