package org.iplantc.agave.client.model;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.iplantc.agave.client.ApiException;
import org.iplantc.agave.client.JsonUtil;
import org.joda.time.DateTime;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AccessToken {
	private String clientKey;
	private String clientSecret;
	private String accessToken;
	private String refreshToken;
	private DateTime createdAt = new DateTime();
	private DateTime expiresAt = new DateTime();
	
	public AccessToken() {}
	
	public AccessToken(String accessToken) 
	{
		setAccessToken(accessToken);
	}
	
	/**
	 * @return the clientKey
	 */
	public String getClientKey() {
		return clientKey;
	}

	/**
	 * @param clientKey the clientKey to set
	 */
	public void setClientKey(String clientKey) {
		this.clientKey = clientKey;
	}

	/**
	 * @return the clientSecret
	 */
	public String getClientSecret() {
		return clientSecret;
	}

	/**
	 * @param clientSecret the clientSecret to set
	 */
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	/**
	 * @return the accessToken
	 */
	public String getAccessToken() {
		return accessToken;
	}

	/**
	 * @param accessToken the accessToken to set
	 */
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	/**
	 * @return the refreshToken
	 */
	public String getRefreshToken() {
		return refreshToken;
	}

	/**
	 * @param refreshToken the refreshToken to set
	 */
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	/**
	 * @return the createdAt
	 */
	public DateTime getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(DateTime createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the expiresAt
	 */
	public DateTime getExpiresAt() {
		return expiresAt;
	}

	/**
	 * @param expiresAt the expiresAt to set
	 */
	public void setExpiresAt(DateTime expiresAt) {
		this.expiresAt = expiresAt;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (o instanceof AccessToken) {
			AccessToken t = (AccessToken)o;
			return StringUtils.equals(accessToken, t.accessToken) &&
				StringUtils.equals(clientKey, t.clientKey) &&
				StringUtils.equals(clientSecret, t.clientSecret) &&
				StringUtils.equals(refreshToken, t.refreshToken);
		} else {
			return false;
		}
	}

	public String toString() {
		return accessToken;
	}
	
	/**
	 * Reads a file from the local files sytem containing a json serialization
	 * of a response from the Agave /token service. Format is as follows:
	 * 
	 * {
	 *	    "access_token": "asdjkflhaskjldfhalskdhfasdaslkd",
	 *	    "apikey": "aksdfhasd_asdfa21341",
	 *	    "apisecret": "moAALSJwasd_L980809sasdudfIOA",
	 *	    "refresh_token": "0987asd0f9a7sdfasodifao07890asd",
	 *		"expires_in": 11297,
	 *		"created_at": 1399931859,
	 *		"username": "nryan"
	 *	}
	 * @param authCacheFile
	 * @return
	 * @throws ApiException
	 */
	public static AccessToken importCache(File authCacheFile) 
	throws ApiException
	{
		if (authCacheFile.exists()) 
		{
			ObjectMapper mapper = new ObjectMapper();
			try {
				JsonNode json = mapper.readTree(authCacheFile);
				if (json.isNull()) {
					throw new ApiException(Status.FORBIDDEN.getStatusCode(), 
							"Local authentication cache file " + authCacheFile.getAbsolutePath() + " is empty.");
				} else if (!json.has("access_token")) {
					throw new ApiException(Status.FORBIDDEN.getStatusCode(), 
							"No access token found in local cache file " + authCacheFile.getAbsolutePath() + "." );
				} else {
					AccessToken token = new AccessToken();
					token.setAccessToken(json.get("access_token").asText());
					token.setClientSecret(json.get("apisecret").asText());
					token.setClientKey(json.get("apikey").asText());
					token.setRefreshToken(json.get("refresh_token").asText());
					token.setCreatedAt(new DateTime(json.get("created_at").asInt()));
					token.setExpiresAt(token.getCreatedAt().plusSeconds(json.get("expires_in").asInt()));
					
					return token;
				}
			} catch(JsonProcessingException e) {
				throw new ApiException(Status.BAD_REQUEST.getStatusCode(), 
						"Unable to parse authentication cache file " + authCacheFile.getAbsolutePath() + " is empty.");
			} catch (IOException e) {
				throw new ApiException(Status.BAD_REQUEST.getStatusCode(), 
						"Unable to parse authentication cache file " + authCacheFile.getAbsolutePath() + " is empty.");
			}
		} else {
			throw new ApiException(Status.FORBIDDEN.getStatusCode(), 
					"Unable to locate authentication token from "
					+ "system cache file " + authCacheFile.getAbsolutePath() + " is empty.");
		}
	}
	
	public void exportCache(File authCacheFile)
	throws IOException
	{
		JsonNode json = JsonUtil.getJsonMapper().createObjectNode()
			.put("access_token", accessToken)
			.put("apisecret", clientSecret)
			.put("apikey", clientKey)
			.put("refresh_token", refreshToken)
			.put("created_at", Math.round(createdAt.getMillis()/1000))
			.put("expires_in",Math.round(expiresAt.getMillis()/1000) - Math.round(new DateTime().getMillis()/1000));
		
		FileUtils.writeStringToFile(authCacheFile, json.toString());
	}
}
