package org.iplantc.agave.client.model;

import java.util.*;
import org.iplantc.agave.client.model.ApplicationOutput;
import org.iplantc.agave.client.model.ApplicationParameter;
import org.iplantc.agave.client.model.ApplicationInput;

public class ApplicationRequest {
	/* Whether the application is available. */
	private Boolean available = null;
	/* Whether the application supports checkpointing. */
	private Boolean checkpointable = null;
	/*
	 * The max execution time that should be used if none is given in a job
	 * description. Ignore if the system does not support schedulers.
	 */
	private String defaultMaxRunTime = null;
	/*
	 * The default memory in GB to pass to the scheduler if none is given in the
	 * job description. This must be less than the max memory parameter in the
	 * target queue definition.
	 */
	private String defaultMemoryPerNode = null;
	/*
	 * The number of nodes that should be used if none is given in a job
	 * description. Ignore if the system does not support schedulers.
	 */
	private String defaultNodeCount = null;
	/*
	 * The number of processors to pass to the scheduler if none are given in
	 * the job description. This must be 1 if the app is serial.
	 */
	private String defaultProcessorsPerNode = null;
	/*
	 * The queue on the execution system that should be used if none is given in
	 * a job description. Ignore if the system does not support schedulers.
	 */
	private String defaultQueue = null;
	/*
	 * The location in the user's default storage system containing the
	 * application wrapper and dependencies.
	 */
	private String deploymentPath = null;
	/* The system id of the storage system where this app should run. */
	private String deploymentSystem = null;
	/* The system id of the execution system where this app should run. */
	private String executionSystem = null;
	/*
	 * The execution type of the application. If you're unsure, it's probably
	 * HPC.
	 */
	private String executionType = null;
	// public enum executionTypeEnum { ATMOSPHERE, HPC, CONDOR, CLI, };
	/* The URL where users can go for more information about the app. */
	private String helpURI = null;
	/* The icon to associate with this app. */
	private String icon = null;
	/* The inputs files for this application. */
	private List<ApplicationInput> inputs = new ArrayList<ApplicationInput>();
	/* The label to use when generating forms. */
	private String label = null;
	/* The full text description of this input to use when generating forms. */
	private String longDescription = null;
	/* An array of modules to load prior to the execution of the application. */
	private List<String> modules = new ArrayList<String>();
	/*
	 * The name of the application. The name does not have to be unique, but the
	 * combination of name and version does.
	 */
	private String name = null;
	/* An array of ontology values describing this application. */
	private List<String> ontology = new ArrayList<String>();
	/* The outputs files for this application. */
	private List<ApplicationOutput> outputs = new ArrayList<ApplicationOutput>();
	/*
	 * The parallelism type of the application. If you're unsure, it's probably
	 * SERIAL.
	 */
	private String parallelism = null;
	// public enum parallelismEnum { SERIAL, PARALLEL, PTHREAD, };
	/* The inputs parameters for this application. */
	private List<ApplicationParameter> parameters = new ArrayList<ApplicationParameter>();
	/* The short description of this application. */
	private String shortDescription = null;
	/* An array of tags related to this application. */
	private List<String> tags = new ArrayList<String>();
	/* The path to the wrapper script relative to the deploymentPath. */
	private String templatePath = null;
	/* The path to the test script relative to the deploymentPath. */
	private String testPath = null;
	/*
	 * The version of the application in #.#.# format. While the version does
	 * not need to be unique, the combination of name and version does have to
	 * be unique.
	 */
	private String version = null;

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	public Boolean getCheckpointable() {
		return checkpointable;
	}

	public void setCheckpointable(Boolean checkpointable) {
		this.checkpointable = checkpointable;
	}

	public String getDefaultMaxRunTime() {
		return defaultMaxRunTime;
	}

	public void setDefaultMaxRunTime(String defaultMaxRunTime) {
		this.defaultMaxRunTime = defaultMaxRunTime;
	}

	public String getDefaultMemoryPerNode() {
		return defaultMemoryPerNode;
	}

	public void setDefaultMemoryPerNode(String defaultMemoryPerNode) {
		this.defaultMemoryPerNode = defaultMemoryPerNode;
	}

	public String getDefaultNodeCount() {
		return defaultNodeCount;
	}

	public void setDefaultNodeCount(String defaultNodeCount) {
		this.defaultNodeCount = defaultNodeCount;
	}

	public String getDefaultProcessorsPerNode() {
		return defaultProcessorsPerNode;
	}

	public void setDefaultProcessorsPerNode(String defaultProcessorsPerNode) {
		this.defaultProcessorsPerNode = defaultProcessorsPerNode;
	}

	public String getDefaultQueue() {
		return defaultQueue;
	}

	public void setDefaultQueue(String defaultQueue) {
		this.defaultQueue = defaultQueue;
	}

	public String getDeploymentPath() {
		return deploymentPath;
	}

	public void setDeploymentPath(String deploymentPath) {
		this.deploymentPath = deploymentPath;
	}

	public String getDeploymentSystem() {
		return deploymentSystem;
	}

	public void setDeploymentSystem(String deploymentSystem) {
		this.deploymentSystem = deploymentSystem;
	}

	public String getExecutionSystem() {
		return executionSystem;
	}

	public void setExecutionSystem(String executionSystem) {
		this.executionSystem = executionSystem;
	}

	public String getExecutionType() {
		return executionType;
	}

	public void setExecutionType(String executionType) {
		this.executionType = executionType;
	}

	public String getHelpURI() {
		return helpURI;
	}

	public void setHelpURI(String helpURI) {
		this.helpURI = helpURI;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public List<ApplicationInput> getInputs() {
		return inputs;
	}

	public void setInputs(List<ApplicationInput> inputs) {
		this.inputs = inputs;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public List<String> getModules() {
		return modules;
	}

	public void setModules(List<String> modules) {
		this.modules = modules;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getOntology() {
		return ontology;
	}

	public void setOntology(List<String> ontology) {
		this.ontology = ontology;
	}

	public List<ApplicationOutput> getOutputs() {
		return outputs;
	}

	public void setOutputs(List<ApplicationOutput> outputs) {
		this.outputs = outputs;
	}

	public String getParallelism() {
		return parallelism;
	}

	public void setParallelism(String parallelism) {
		this.parallelism = parallelism;
	}

	public List<ApplicationParameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<ApplicationParameter> parameters) {
		this.parameters = parameters;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getTemplatePath() {
		return templatePath;
	}

	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}

	public String getTestPath() {
		return testPath;
	}

	public void setTestPath(String testPath) {
		this.testPath = testPath;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ApplicationRequest {\n");
		sb.append("  available: ").append(available).append("\n");
		sb.append("  checkpointable: ").append(checkpointable).append("\n");
		sb.append("  defaultMaxRunTime: ").append(defaultMaxRunTime)
				.append("\n");
		sb.append("  defaultMemoryPerNode: ").append(defaultMemoryPerNode)
				.append("\n");
		sb.append("  defaultNodeCount: ").append(defaultNodeCount).append("\n");
		sb.append("  defaultProcessorsPerNode: ")
				.append(defaultProcessorsPerNode).append("\n");
		sb.append("  defaultQueue: ").append(defaultQueue).append("\n");
		sb.append("  deploymentPath: ").append(deploymentPath).append("\n");
		sb.append("  deploymentSystem: ").append(deploymentSystem).append("\n");
		sb.append("  executionSystem: ").append(executionSystem).append("\n");
		sb.append("  executionType: ").append(executionType).append("\n");
		sb.append("  helpURI: ").append(helpURI).append("\n");
		sb.append("  icon: ").append(icon).append("\n");
		sb.append("  inputs: ").append(inputs).append("\n");
		sb.append("  label: ").append(label).append("\n");
		sb.append("  longDescription: ").append(longDescription).append("\n");
		sb.append("  modules: ").append(modules).append("\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("  ontology: ").append(ontology).append("\n");
		sb.append("  outputs: ").append(outputs).append("\n");
		sb.append("  parallelism: ").append(parallelism).append("\n");
		sb.append("  parameters: ").append(parameters).append("\n");
		sb.append("  shortDescription: ").append(shortDescription).append("\n");
		sb.append("  tags: ").append(tags).append("\n");
		sb.append("  templatePath: ").append(templatePath).append("\n");
		sb.append("  testPath: ").append(testPath).append("\n");
		sb.append("  version: ").append(version).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
