package org.iplantc.agave.client.model;

import org.iplantc.agave.client.model.StoredCredential;

public class SystemCredentialsResponse {
	/* success or failure */
	private String message = null;
	private StoredCredential result = null;
	/* success or failure */
	private String status = null;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public StoredCredential getResult() {
		return result;
	}

	public void setResult(StoredCredential result) {
		this.result = result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class SystemCredentialsResponse {\n");
		sb.append("  message: ").append(message).append("\n");
		sb.append("  result: ").append(result).append("\n");
		sb.append("  status: ").append(status).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
