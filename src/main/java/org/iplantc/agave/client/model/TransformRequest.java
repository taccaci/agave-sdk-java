package org.iplantc.agave.client.model;

public class TransformRequest {
	/*
	 * The original file type of the file. If not given, the file type is
	 * assumed to be raw.
	 */
	private String nativeFormat = null;
	/* The uri to which the transformed file will be staged. */
	private String url = null;
	/*
	 * The URI to notify when the transfer is complete. This can be an email
	 * address or http URL. If a URL is given, a GET will be made to this
	 * address. URL templating is supported. Valid template values are: ${NAME},
	 * ${SOURCE_FORMAT}, ${DEST_FORMAT}, ${STATUS}
	 */
	private String callbackUrl = null;

	public String getNativeFormat() {
		return nativeFormat;
	}

	public void setNativeFormat(String nativeFormat) {
		this.nativeFormat = nativeFormat;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class TransformRequest {\n");
		sb.append("  nativeFormat: ").append(nativeFormat).append("\n");
		sb.append("  url: ").append(url).append("\n");
		sb.append("  callbackUrl: ").append(callbackUrl).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
