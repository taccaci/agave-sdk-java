package org.iplantc.agave.client.model;

import org.iplantc.agave.client.model.UserCredential;

public class LoginConfig {
	/* The default authentication credential used for this system. */
	private UserCredential auth = null;
	/* The hostname or ip address of the submission server */
	private String host = null;
	/* The port number of the submission server. */
	private Integer port = null;
	/* The protocol used to authenticate to the submission server. */
	private String protocol = null;

	// public enum protocolEnum { GSISSH, SSH, LOCAL, };
	public UserCredential getAuth() {
		return auth;
	}

	public void setAuth(UserCredential auth) {
		this.auth = auth;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class LoginConfig {\n");
		sb.append("  auth: ").append(auth).append("\n");
		sb.append("  host: ").append(host).append("\n");
		sb.append("  port: ").append(port).append("\n");
		sb.append("  protocol: ").append(protocol).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
