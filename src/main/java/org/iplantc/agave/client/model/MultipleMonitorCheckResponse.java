package org.iplantc.agave.client.model;

import java.util.*;
import org.iplantc.agave.client.model.MonitorCheck;

public class MultipleMonitorCheckResponse {
	/* Description of an error. null otherwise. */
	private String message = null;
	/* response body */
	private List<MonitorCheck> result = new ArrayList<MonitorCheck>();
	/* success or failure */
	private String status = null;
	/* API version number */
	private String version = null;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<MonitorCheck> getResult() {
		return result;
	}

	public void setResult(List<MonitorCheck> result) {
		this.result = result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class MultipleMonitorCheckResponse {\n");
		sb.append("  message: ").append(message).append("\n");
		sb.append("  result: ").append(result).append("\n");
		sb.append("  status: ").append(status).append("\n");
		sb.append("  version: ").append(version).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
