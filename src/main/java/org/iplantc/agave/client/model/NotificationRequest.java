package org.iplantc.agave.client.model;

public class NotificationRequest {
	/* UUID of resource to whome the event applies. */
	private String associatedUuid = null;
	/* The url or email address that will be notified of the event. */
	private String url = null;
	/*
	 * Whether this notification should stay active after it fires the first
	 * time.
	 */
	private Boolean persistent = null;

	public String getAssociatedUuid() {
		return associatedUuid;
	}

	public void setAssociatedUuid(String associatedUuid) {
		this.associatedUuid = associatedUuid;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Boolean getPersistent() {
		return persistent;
	}

	public void setPersistent(Boolean persistent) {
		this.persistent = persistent;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class NotificationRequest {\n");
		sb.append("  associatedUuid: ").append(associatedUuid).append("\n");
		sb.append("  url: ").append(url).append("\n");
		sb.append("  persistent: ").append(persistent).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
