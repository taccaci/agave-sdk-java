package org.iplantc.agave.client.model;

import java.util.*;
import org.iplantc.agave.client.model.Permission;

public class PermissionListing {
	/* Unique name of the application to which this permissions apply. */
	private String name = null;
	/* Owner of the application. */
	private String owner = null;
	/* Permissions granted on this application. */
	private List<Permission> permissions = new ArrayList<Permission>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class PermissionListing {\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("  owner: ").append(owner).append("\n");
		sb.append("  permissions: ").append(permissions).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
