package org.iplantc.agave.client.model;

import java.util.*;
import org.iplantc.agave.client.model.ApplicationPermission;

public class ApplicationPermissionResponse {
	/* success or failure */
	private String message = null;
	/* An array of permission objects for this application. */
	private List<ApplicationPermission> result = new ArrayList<ApplicationPermission>();
	/* success or failure */
	private String status = null;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<ApplicationPermission> getResult() {
		return result;
	}

	public void setResult(List<ApplicationPermission> result) {
		this.result = result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ApplicationPermissionResponse {\n");
		sb.append("  message: ").append(message).append("\n");
		sb.append("  result: ").append(result).append("\n");
		sb.append("  status: ").append(status).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
