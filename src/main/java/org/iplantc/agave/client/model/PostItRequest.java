package org.iplantc.agave.client.model;

public class PostItRequest {
	/* The url that will be invoked when the PostIt is redeemed. */
	private String url = null;
	/* The username of the internal user attached to this PostIt. */
	private String internalUsername = null;
	/* The method that will be invoked when the PostIt is redeemed. */
	private String method = null;
	// public enum methodEnum { GET, PUT, POST, DELETE, };
	/*
	 * The maximum lifetime in seconds of this PostIt on this token. Defaults to
	 * 2592000 (30 days)
	 */
	private Integer lifetime = null;
	/*
	 * The maximum number of invocations remaining on this PostIt. Defaults to
	 * no limit
	 */
	private Integer maxUses = null;
	/*
	 * If true, the provided url will be called without authentication. Default
	 * is false
	 */
	private Boolean noauth = null;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getInternalUsername() {
		return internalUsername;
	}

	public void setInternalUsername(String internalUsername) {
		this.internalUsername = internalUsername;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Integer getLifetime() {
		return lifetime;
	}

	public void setLifetime(Integer lifetime) {
		this.lifetime = lifetime;
	}

	public Integer getMaxUses() {
		return maxUses;
	}

	public void setMaxUses(Integer maxUses) {
		this.maxUses = maxUses;
	}

	public Boolean getNoauth() {
		return noauth;
	}

	public void setNoauth(Boolean noauth) {
		this.noauth = noauth;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class PostItRequest {\n");
		sb.append("  url: ").append(url).append("\n");
		sb.append("  internalUsername: ").append(internalUsername).append("\n");
		sb.append("  method: ").append(method).append("\n");
		sb.append("  lifetime: ").append(lifetime).append("\n");
		sb.append("  maxUses: ").append(maxUses).append("\n");
		sb.append("  noauth: ").append(noauth).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
