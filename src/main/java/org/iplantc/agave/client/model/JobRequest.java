package org.iplantc.agave.client.model;

import java.util.*;
import org.iplantc.agave.client.model.JobParameters;
import org.iplantc.agave.client.model.JobInputs;
import org.iplantc.agave.client.model.Notification;

public class JobRequest {
	/*
	 * The unique name of the application being run by this job. This must be a
	 * valid application that the calling user has permission to run.
	 */
	private String appId = null;
	/*
	 * Whether the output from this job should be archived. If true, all new
	 * files created by this application's execution will be archived to the
	 * archivePath in the user's default storage system.
	 */
	private Boolean archive = null;
	/*
	 * The path of the archive folder for this job on the user's default storage
	 * sytem.
	 */
	private String archivePath = null;
	/*
	 * The unique id of the storage system on which this job's output will be
	 * staged.
	 */
	private String archiveSystem = null;
	/*
	 * The queue to which this job should be submitted. This is optional and
	 * only applies when the execution system has a batch scheduler.
	 */
	private String batchQueue = null;
	/*
	 * The application specific input files needed for this job. These vary from
	 * application to application and should be entered as multiple individual
	 * parameters in the form. Inputs may be given as relative paths in the
	 * user's default storage system or as URI. If a URI is given, the data will
	 * be staged in by the IO service and made avaialble to the application at
	 * run time.
	 */
	private JobInputs inputs = null;
	/* The requested memory for this application to run given in GB. */
	private String memoryPerNode = null;
	/* The name of the job. */
	private String name = null;
	/*
	 * The number of processors this application should utilize while running.
	 * If the application is not of executionType PARALLEL, this should be 1.
	 */
	private Integer nodeCount = null;
	/* An array of notifications you wish to receive. */
	private List<Notification> notifications = new ArrayList<Notification>();
	/*
	 * The application specific parameters needed for this job. These vary from
	 * application to application and should be entered as multiple individual
	 * parameters in the form. The actual dataType will be determined by the
	 * application description.
	 */
	private JobParameters parameters = null;
	/*
	 * The number of processors this application should utilize while running.
	 * If the application is not of executionType PARALLEL, this should be 1.
	 */
	private Integer processorsPerNode = null;
	/*
	 * The requested compute time needed for this application to complete given
	 * in HH:mm:ss format.
	 */
	private String maxRunTime = null;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public Boolean getArchive() {
		return archive;
	}

	public void setArchive(Boolean archive) {
		this.archive = archive;
	}

	public String getArchivePath() {
		return archivePath;
	}

	public void setArchivePath(String archivePath) {
		this.archivePath = archivePath;
	}

	public String getArchiveSystem() {
		return archiveSystem;
	}

	public void setArchiveSystem(String archiveSystem) {
		this.archiveSystem = archiveSystem;
	}

	public String getBatchQueue() {
		return batchQueue;
	}

	public void setBatchQueue(String batchQueue) {
		this.batchQueue = batchQueue;
	}

	public JobInputs getInputs() {
		return inputs;
	}

	public void setInputs(JobInputs inputs) {
		this.inputs = inputs;
	}

	public String getMemoryPerNode() {
		return memoryPerNode;
	}

	public void setMemoryPerNode(String memoryPerNode) {
		this.memoryPerNode = memoryPerNode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNodeCount() {
		return nodeCount;
	}

	public void setNodeCount(Integer nodeCount) {
		this.nodeCount = nodeCount;
	}

	public List<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	public JobParameters getParameters() {
		return parameters;
	}

	public void setParameters(JobParameters parameters) {
		this.parameters = parameters;
	}

	public Integer getProcessorsPerNode() {
		return processorsPerNode;
	}

	public void setProcessorsPerNode(Integer processorsPerNode) {
		this.processorsPerNode = processorsPerNode;
	}

	public String getMaxRunTime() {
		return maxRunTime;
	}

	public void setMaxRunTime(String maxRunTime) {
		this.maxRunTime = maxRunTime;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class JobRequest {\n");
		sb.append("  appId: ").append(appId).append("\n");
		sb.append("  archive: ").append(archive).append("\n");
		sb.append("  archivePath: ").append(archivePath).append("\n");
		sb.append("  archiveSystem: ").append(archiveSystem).append("\n");
		sb.append("  batchQueue: ").append(batchQueue).append("\n");
		sb.append("  inputs: ").append(inputs).append("\n");
		sb.append("  memoryPerNode: ").append(memoryPerNode).append("\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("  nodeCount: ").append(nodeCount).append("\n");
		sb.append("  notifications: ").append(notifications).append("\n");
		sb.append("  parameters: ").append(parameters).append("\n");
		sb.append("  processorsPerNode: ").append(processorsPerNode)
				.append("\n");
		sb.append("  maxRunTime: ").append(maxRunTime).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
