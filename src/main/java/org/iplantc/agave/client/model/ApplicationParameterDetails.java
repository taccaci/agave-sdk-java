package org.iplantc.agave.client.model;

public class ApplicationParameterDetails {
	/* Description of this parameter. */
	private String description = null;
	/* The label for this parameter */
	private String label = null;
	/* The command line value of this parameter (ex -n, --name, -name, etc) */
	private String argument = null;
	/* Whether the argument value should be passed into the wrapper at run time */
	private Boolean showArgument = null;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getArgument() {
		return argument;
	}

	public void setArgument(String argument) {
		this.argument = argument;
	}

	public Boolean getShowArgument() {
		return showArgument;
	}

	public void setShowArgument(Boolean showArgument) {
		this.showArgument = showArgument;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ApplicationParameterDetails {\n");
		sb.append("  description: ").append(description).append("\n");
		sb.append("  label: ").append(label).append("\n");
		sb.append("  argument: ").append(argument).append("\n");
		sb.append("  showArgument: ").append(showArgument).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
