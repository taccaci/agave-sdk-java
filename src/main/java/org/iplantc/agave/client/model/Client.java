package org.iplantc.agave.client.model;

public class Client {
	/* The API key for this client. */
	private String key = null;
	/* The API secret for this client. */
	private String secret = null;
	/* Callback URL for OAuth authentication grant. */
	private String callbackUrl = null;
	/*
	 * Description of the client. This will be shown to users when
	 * authentication via OAuth web flows
	 */
	private String description = null;
	/* The name of the client. */
	private String name = null;
	/* The access tier for this client. */
	private String tier = null;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTier() {
		return tier;
	}

	public void setTier(String tier) {
		this.tier = tier;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Client {\n");
		sb.append("  key: ").append(key).append("\n");
		sb.append("  secret: ").append(secret).append("\n");
		sb.append("  callbackUrl: ").append(callbackUrl).append("\n");
		sb.append("  description: ").append(description).append("\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("  tier: ").append(tier).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
