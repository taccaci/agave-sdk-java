package org.iplantc.agave.client.model;

import org.joda.time.DateTime;

public class MonitorCheck {
	/* A timestamp indicating when this monitor check was created. */
	private DateTime created = null;
	/* The UUID for this monitor check. */
	private String id = null;
	/* The error message if this monitor check failed. */
	private String message = null;
	/* The results of this monitor check. */
	private String result = null;

	// public enum resultEnum { PASSED, FAILED, UNKNOWN, };
	public DateTime getCreated() {
		return created;
	}

	public void setCreated(DateTime created) {
		this.created = created;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class MonitorCheck {\n");
		sb.append("  created: ").append(created).append("\n");
		sb.append("  id: ").append(id).append("\n");
		sb.append("  message: ").append(message).append("\n");
		sb.append("  result: ").append(result).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
