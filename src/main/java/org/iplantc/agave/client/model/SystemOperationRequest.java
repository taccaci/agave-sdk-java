package org.iplantc.agave.client.model;

public class SystemOperationRequest {
	/* Action to perform on the system. */
	private String action = null;
	// public enum actionEnum { PUBLISH, UNPUBLISH, SETDEFAULT, UNSETDEFAULT,
	// SETGLOBALDEFAULT, UNSETGLOBALDEFAULT, CLONE, };
	/* The new system id of the cloned system */
	private String id = null;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class SystemOperationRequest {\n");
		sb.append("  action: ").append(action).append("\n");
		sb.append("  id: ").append(id).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
