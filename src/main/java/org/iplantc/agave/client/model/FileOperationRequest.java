package org.iplantc.agave.client.model;

public class FileOperationRequest {
	/* Action to perform on the file or folder. */
	private String action = null;
	// public enum actionEnum { mkdir, rename, copy, move, };
	/* Name of new directory. Only used with the mkdir action. */
	private String dirName = null;
	/* New name of the file or folder. Only used with the rename action. */
	private String newName = null;
	/*
	 * Destination to which to copy the file or folder. Only used with the copy
	 * action.
	 */
	private String destPath = null;
	/*
	 * Destination to which to move the file or folder. Only used with the move
	 * action.
	 */
	private String newPath = null;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDirName() {
		return dirName;
	}

	public void setDirName(String dirName) {
		this.dirName = dirName;
	}

	public String getNewName() {
		return newName;
	}

	public void setNewName(String newName) {
		this.newName = newName;
	}

	public String getDestPath() {
		return destPath;
	}

	public void setDestPath(String destPath) {
		this.destPath = destPath;
	}

	public String getNewPath() {
		return newPath;
	}

	public void setNewPath(String newPath) {
		this.newPath = newPath;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class FileOperationRequest {\n");
		sb.append("  action: ").append(action).append("\n");
		sb.append("  dirName: ").append(dirName).append("\n");
		sb.append("  newName: ").append(newName).append("\n");
		sb.append("  destPath: ").append(destPath).append("\n");
		sb.append("  newPath: ").append(newPath).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
