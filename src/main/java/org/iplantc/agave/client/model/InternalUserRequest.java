package org.iplantc.agave.client.model;

public class InternalUserRequest {
	/* The api user's unique username. */
	private String username = null;
	/* The api user's unique email address. */
	private String email = null;
	/* The api user's city. */
	private String city = null;
	/* The api user's country. */
	private String country = null;
	/* The api user's institutional department. */
	private String department = null;
	/* The api user's fax number. */
	private String fax = null;
	/* The api user's first name. */
	private String firstName = null;
	/* The api user's gender. male or female. */
	private String gender = null;
	// public enum genderEnum { male, female, };
	/* The api user's home institution */
	private String institution = null;
	/* The api user's last name. */
	private String lastName = null;
	/* The api user's phone number. */
	private String phone = null;
	/* The api user's position of employment. */
	private String position = null;
	/* The api user's primary area of research. */
	private String researchArea = null;
	/* The api user's state. */
	private String state = null;
	/* The api user's status. active or deleted. */
	private String status = null;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getResearchArea() {
		return researchArea;
	}

	public void setResearchArea(String researchArea) {
		this.researchArea = researchArea;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class InternalUserRequest {\n");
		sb.append("  username: ").append(username).append("\n");
		sb.append("  email: ").append(email).append("\n");
		sb.append("  city: ").append(city).append("\n");
		sb.append("  country: ").append(country).append("\n");
		sb.append("  department: ").append(department).append("\n");
		sb.append("  fax: ").append(fax).append("\n");
		sb.append("  firstName: ").append(firstName).append("\n");
		sb.append("  gender: ").append(gender).append("\n");
		sb.append("  institution: ").append(institution).append("\n");
		sb.append("  lastName: ").append(lastName).append("\n");
		sb.append("  phone: ").append(phone).append("\n");
		sb.append("  position: ").append(position).append("\n");
		sb.append("  researchArea: ").append(researchArea).append("\n");
		sb.append("  state: ").append(state).append("\n");
		sb.append("  status: ").append(status).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
