package org.iplantc.agave.client.model;

import org.iplantc.agave.client.model.ApplicationParameterDetails;
import org.iplantc.agave.client.model.ApplicationParameterValue;
import org.iplantc.agave.client.model.ApplicationParameterOntology;

public class ApplicationParameter {
	/* The details for this parameter. */
	private ApplicationParameterDetails details = null;
	/*
	 * The id of this parameter. This will be the replacement string in your
	 * wrapper scripts.
	 */
	private String id = null;
	/* The ontologies for this parameter. */
	private ApplicationParameterOntology semantics = null;
	/* The inputs files for this parameter. */
	private ApplicationParameterValue value = null;

	public ApplicationParameterDetails getDetails() {
		return details;
	}

	public void setDetails(ApplicationParameterDetails details) {
		this.details = details;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ApplicationParameterOntology getSemantics() {
		return semantics;
	}

	public void setSemantics(ApplicationParameterOntology semantics) {
		this.semantics = semantics;
	}

	public ApplicationParameterValue getValue() {
		return value;
	}

	public void setValue(ApplicationParameterValue value) {
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ApplicationParameter {\n");
		sb.append("  details: ").append(details).append("\n");
		sb.append("  id: ").append(id).append("\n");
		sb.append("  semantics: ").append(semantics).append("\n");
		sb.append("  value: ").append(value).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
