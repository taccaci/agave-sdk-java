package org.iplantc.agave.client.model;

public class ACL {
	/* can read */
	private Boolean read = null;
	/* can write */
	private Boolean write = null;

	public Boolean getRead() {
		return read;
	}

	public void setRead(Boolean read) {
		this.read = read;
	}

	public Boolean getWrite() {
		return write;
	}

	public void setWrite(Boolean write) {
		this.write = write;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ACL {\n");
		sb.append("  read: ").append(read).append("\n");
		sb.append("  write: ").append(write).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
