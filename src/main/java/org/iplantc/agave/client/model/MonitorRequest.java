package org.iplantc.agave.client.model;

public class MonitorRequest {
	/* Whether this monitor is currently active. */
	private Boolean active = null;
	/*
	 * The interval in minutes on which this monitor will run. Minimum is 5.
	 * Default is 720.
	 */
	private Integer frequency = null;
	/* Internal user account used to perform the check. */
	private String internalUsername = null;
	/*
	 * The id of the sytem to be monitored. This must be an active system
	 * registered with the Systems service.
	 */
	private String target = null;
	/*
	 * Whether this Monitor should update the system status when the results
	 * change. You must have the ADMIN role on the target system to use this
	 * feature.
	 */
	private Boolean updateSystemStatus = null;

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}

	public String getInternalUsername() {
		return internalUsername;
	}

	public void setInternalUsername(String internalUsername) {
		this.internalUsername = internalUsername;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Boolean getUpdateSystemStatus() {
		return updateSystemStatus;
	}

	public void setUpdateSystemStatus(Boolean updateSystemStatus) {
		this.updateSystemStatus = updateSystemStatus;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class MonitorRequest {\n");
		sb.append("  active: ").append(active).append("\n");
		sb.append("  frequency: ").append(frequency).append("\n");
		sb.append("  internalUsername: ").append(internalUsername).append("\n");
		sb.append("  target: ").append(target).append("\n");
		sb.append("  updateSystemStatus: ").append(updateSystemStatus)
				.append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
