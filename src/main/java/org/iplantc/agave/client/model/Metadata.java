package org.iplantc.agave.client.model;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

public class Metadata {
	/*
	 * UUIDs of associated Agave entities, including the Data to which this
	 * Metadata belongs.
	 */
	private List<String> associationIds = new ArrayList<String>();
	/*
	 * A timestamp indicating when this Metadata was created in the metadata
	 * store.
	 */
	private DateTime created = null;
	/* The name of the Internal User, if any, who owns this metadata. */
	private String internalUsername = null;
	/*
	 * A timestamp indicating when this Metadata was last updated in the
	 * metadata store.
	 */
	private DateTime lastUpdated = null;
	/* The name of this metadata */
	private String name = null;
	/* The API user who owns this Metadata. */
	private String owner = null;
	/* The UUID for this Metadata. */
	private String uuid = null;
	/*
	 * A free text or JSON string containing the metadata stored for the given
	 * associationIds
	 */
	private String value = null;

	public List<String> getAssociationIds() {
		return associationIds;
	}

	public void setAssociationIds(List<String> associationIds) {
		this.associationIds = associationIds;
	}

	public DateTime getCreated() {
		return created;
	}

	public void setCreated(DateTime created) {
		this.created = created;
	}

	public String getInternalUsername() {
		return internalUsername;
	}

	public void setInternalUsername(String internalUsername) {
		this.internalUsername = internalUsername;
	}

	public DateTime getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(DateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Metadata {\n");
		sb.append("  associationIds: ").append(associationIds).append("\n");
		sb.append("  created: ").append(created).append("\n");
		sb.append("  internalUsername: ").append(internalUsername).append("\n");
		sb.append("  lastUpdated: ").append(lastUpdated).append("\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("  owner: ").append(owner).append("\n");
		sb.append("  uuid: ").append(uuid).append("\n");
		sb.append("  value: ").append(value).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
