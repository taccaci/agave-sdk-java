package org.iplantc.agave.client.model;

import org.joda.time.DateTime;

public class Notification {
	/* UUID of resource to whome the event applies. */
	private String associatedUuid = null;
	/* The number of times this notification has been attempted to be fulfilled. */
	private Integer attempts = null;
	/*
	 * A timestamp indicating when this notification was created in the
	 * notification store.
	 */
	private DateTime created = null;
	/* A timestamp indicating the last time this notification was sent. */
	private DateTime lastSent = null;
	/* The API user who owns this notification. */
	private String owner = null;
	/*
	 * Whether this notification should stay active after it fires the first
	 * time.
	 */
	private Boolean persistent = null;
	/* The response code from POSTing to the url or sending an email. */
	private Integer responseCode = null;
	/* Whether this notification was sent successfully. */
	private Boolean success = null;
	/* The url or email address that will be notified of the event. */
	private String url = null;
	/* The UUID for this notification. */
	private String uuid = null;

	public String getAssociatedUuid() {
		return associatedUuid;
	}

	public void setAssociatedUuid(String associatedUuid) {
		this.associatedUuid = associatedUuid;
	}

	public Integer getAttempts() {
		return attempts;
	}

	public void setAttempts(Integer attempts) {
		this.attempts = attempts;
	}

	public DateTime getCreated() {
		return created;
	}

	public void setCreated(DateTime created) {
		this.created = created;
	}

	public DateTime getLastSent() {
		return lastSent;
	}

	public void setLastSent(DateTime lastSent) {
		this.lastSent = lastSent;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Boolean getPersistent() {
		return persistent;
	}

	public void setPersistent(Boolean persistent) {
		this.persistent = persistent;
	}

	public Integer getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Notification {\n");
		sb.append("  associatedUuid: ").append(associatedUuid).append("\n");
		sb.append("  attempts: ").append(attempts).append("\n");
		sb.append("  created: ").append(created).append("\n");
		sb.append("  lastSent: ").append(lastSent).append("\n");
		sb.append("  owner: ").append(owner).append("\n");
		sb.append("  persistent: ").append(persistent).append("\n");
		sb.append("  responseCode: ").append(responseCode).append("\n");
		sb.append("  success: ").append(success).append("\n");
		sb.append("  url: ").append(url).append("\n");
		sb.append("  uuid: ").append(uuid).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
