package org.iplantc.agave.client.model;

public class SystemSummary {
	/* Verbose description of this system. */
	private String description = null;
	/* Unique identifier for this system. */
	private String id = null;
	/* Is the system the default for the authenticated user? */
	private Boolean isDefault = null;
	/* Is the system publicly available? */
	private Boolean isPublic = null;
	/* Common name for this system. */
	private String name = null;
	/* The status of this system. Systems must be in UP status to be used. */
	private String status = null;
	// public enum statusEnum { UP, DOWN, UNKNOWN, };
	/* The type of this system. */
	private String type = null;

	// public enum typeEnum { EXECUTION, STORAGE, };
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Boolean getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Boolean isPublic) {
		this.isPublic = isPublic;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class SystemSummary {\n");
		sb.append("  description: ").append(description).append("\n");
		sb.append("  id: ").append(id).append("\n");
		sb.append("  isDefault: ").append(isDefault).append("\n");
		sb.append("  isPublic: ").append(isPublic).append("\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("  status: ").append(status).append("\n");
		sb.append("  type: ").append(type).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
