package org.iplantc.agave.client.model;

public class SystemRole {
	/* The username of the api user granted this role. */
	private String username = null;
	/* The role granted this user. */
	private String role = null;

	// public enum roleEnum { USER, PUBLISHER, ADMIN, OWNER, };
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class SystemRole {\n");
		sb.append("  username: ").append(username).append("\n");
		sb.append("  role: ").append(role).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
