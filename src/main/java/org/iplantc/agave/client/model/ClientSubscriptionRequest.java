package org.iplantc.agave.client.model;

public class ClientSubscriptionRequest {
	/* The name of the API. */
	private String apiName = null;
	/* The user who registered the API. */
	private String apiProvider = null;
	/*
	 * The current major version of the API. This is appended to the api_context
	 * to create the base API url.
	 */
	private String apiVersion = null;
	/* The access tier for this client. */
	private String tier = null;

	// public enum tierEnum { UNLIMITED, GOLD, SILVER, BRONZE, };
	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String getApiProvider() {
		return apiProvider;
	}

	public void setApiProvider(String apiProvider) {
		this.apiProvider = apiProvider;
	}

	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	public String getTier() {
		return tier;
	}

	public void setTier(String tier) {
		this.tier = tier;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ClientSubscriptionRequest {\n");
		sb.append("  apiName: ").append(apiName).append("\n");
		sb.append("  apiProvider: ").append(apiProvider).append("\n");
		sb.append("  apiVersion: ").append(apiVersion).append("\n");
		sb.append("  tier: ").append(tier).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
