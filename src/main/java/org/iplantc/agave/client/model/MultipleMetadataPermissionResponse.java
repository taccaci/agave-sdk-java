package org.iplantc.agave.client.model;

import java.util.*;
import org.iplantc.agave.client.model.Permission;

public class MultipleMetadataPermissionResponse {
	/* Error message caused by this request */
	private String message = null;
	/* Permissions granted on this metadata resource. */
	private List<Permission> result = new ArrayList<Permission>();
	/* success or failure */
	private String status = null;
	/* API version number */
	private String version = null;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Permission> getResult() {
		return result;
	}

	public void setResult(List<Permission> result) {
		this.result = result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class MultipleMetadataPermissionResponse {\n");
		sb.append("  message: ").append(message).append("\n");
		sb.append("  result: ").append(result).append("\n");
		sb.append("  status: ").append(status).append("\n");
		sb.append("  version: ").append(version).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
