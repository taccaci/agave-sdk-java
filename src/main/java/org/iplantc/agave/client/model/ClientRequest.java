package org.iplantc.agave.client.model;

public class ClientRequest {
	/* The name of the client. */
	private String name = null;
	/*
	 * Description of the client. This will be shown to users when
	 * authentication via OAuth web flows
	 */
	private String description = null;
	/* The access tier for this client. */
	private String tier = null;
	// public enum tierEnum { UNLIMITED, GOLD, SILVER, BRONZE, };
	/* Callback URL for OAuth authentication grant. */
	private String callbackUrl = null;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTier() {
		return tier;
	}

	public void setTier(String tier) {
		this.tier = tier;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ClientRequest {\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("  description: ").append(description).append("\n");
		sb.append("  tier: ").append(tier).append("\n");
		sb.append("  callbackUrl: ").append(callbackUrl).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
