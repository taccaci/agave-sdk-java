package org.iplantc.agave.client.model;

import java.util.Date;

public class PostIt {
	/* The creation date in ISO 8601 format. */
	private Date created = null;
	/* The api user who made the PostIt creation request. */
	private String creator = null;
	/* The expiration date in ISO 8601 format. */
	private Date expires = null;
	/* The username of the internal user attached to this PostIt. */
	private String internalUsername = null;
	/* The url that will be invoked when the PostIt is redeemed. */
	private String method = null;
	/* The PostIt nonce. */
	private String postit = null;
	/* Whether the resulting call should be authenticated */
	private Boolean noauth = true;
	/* The number of invocations remaining on this token. */
	private Integer remainingUses = null;
	/* The url that will be invoked when the PostIt is redeemed. */
	private String url = null;

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getExpires() {
		return expires;
	}

	public void setExpires(Date expires) {
		this.expires = expires;
	}

	public String getInternalUsername() {
		return internalUsername;
	}

	public void setInternalUsername(String internalUsername) {
		this.internalUsername = internalUsername;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getPostit() {
		return postit;
	}

	public void setPostit(String postit) {
		this.postit = postit;
	}

	public Boolean getNoauth() {
		return noauth;
	}

	public void setNoauth(Boolean noauth) {
		this.noauth = noauth;
	}

	public Integer getRemainingUses() {
		return remainingUses;
	}

	public void setRemainingUses(Integer remainingUses) {
		this.remainingUses = remainingUses;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class PostIt {\n");
		sb.append("  created: ").append(created).append("\n");
		sb.append("  creator: ").append(creator).append("\n");
		sb.append("  expires: ").append(expires).append("\n");
		sb.append("  internalUsername: ").append(internalUsername).append("\n");
		sb.append("  method: ").append(method).append("\n");
		sb.append("  postit: ").append(postit).append("\n");
		sb.append("  noauth: ").append(getNoauth()).append("\n");
		sb.append("  remainingUses: ").append(remainingUses).append("\n");
		sb.append("  url: ").append(url).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
