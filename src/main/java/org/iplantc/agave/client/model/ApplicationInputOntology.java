package org.iplantc.agave.client.model;

import java.util.*;

public class ApplicationInputOntology {
	/* The file types acceptable for this input. */
	private List<String> fileTypes = new ArrayList<String>();
	/* The minimum number of times this input may appear. */
	private Integer minCardinality = null;
	private List<String> ontology = new ArrayList<String>();

	public List<String> getFileTypes() {
		return fileTypes;
	}

	public void setFileTypes(List<String> fileTypes) {
		this.fileTypes = fileTypes;
	}

	public Integer getMinCardinality() {
		return minCardinality;
	}

	public void setMinCardinality(Integer minCardinality) {
		this.minCardinality = minCardinality;
	}

	public List<String> getOntology() {
		return ontology;
	}

	public void setOntology(List<String> ontology) {
		this.ontology = ontology;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ApplicationInputOntology {\n");
		sb.append("  fileTypes: ").append(fileTypes).append("\n");
		sb.append("  minCardinality: ").append(minCardinality).append("\n");
		sb.append("  ontology: ").append(ontology).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
