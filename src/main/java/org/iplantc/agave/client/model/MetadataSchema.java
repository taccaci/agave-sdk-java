package org.iplantc.agave.client.model;

import org.joda.time.DateTime;

public class MetadataSchema {
	/*
	 * A timestamp indicating when this Metadata was created in the metadata
	 * schema store.
	 */
	private DateTime created = null;
	/* The name of the Internal User, if any, who owns this schema. */
	private String internalUsername = null;
	/*
	 * A timestamp indicating when this Metadata was last updated in the
	 * metadata schema store.
	 */
	private DateTime lastUpdated = null;
	/* The API user who owns this Schema. */
	private String owner = null;
	/* A JSON Schema */
	private String schema = null;
	/* The UUID for this Schema. */
	private String uuid = null;

	public DateTime getCreated() {
		return created;
	}

	public void setCreated(DateTime created) {
		this.created = created;
	}

	public String getInternalUsername() {
		return internalUsername;
	}

	public void setInternalUsername(String internalUsername) {
		this.internalUsername = internalUsername;
	}

	public DateTime getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(DateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class MetadataSchema {\n");
		sb.append("  created: ").append(created).append("\n");
		sb.append("  internalUsername: ").append(internalUsername).append("\n");
		sb.append("  lastUpdated: ").append(lastUpdated).append("\n");
		sb.append("  owner: ").append(owner).append("\n");
		sb.append("  schema: ").append(schema).append("\n");
		sb.append("  uuid: ").append(uuid).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
