package org.iplantc.agave.client.model;

import java.util.*;
import org.iplantc.agave.client.model.Permission;

public class FilePermission {
	/* The name of the file/folder. */
	private String name = null;
	/* Local username of the owner. */
	private String owner = null;
	/* One or more permission objects */
	private List<Permission> permissions = new ArrayList<Permission>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class FilePermission {\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("  owner: ").append(owner).append("\n");
		sb.append("  permissions: ").append(permissions).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
