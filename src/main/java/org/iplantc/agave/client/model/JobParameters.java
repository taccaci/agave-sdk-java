package org.iplantc.agave.client.model;

public class JobParameters {
	/*
	 * One or more parameters identified in the description of the app you're
	 * running.
	 */
	private String parameter1 = null;

	public String getParameter1() {
		return parameter1;
	}

	public void setParameter1(String parameter1) {
		this.parameter1 = parameter1;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class JobParameters {\n");
		sb.append("  parameter1: ").append(parameter1).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
