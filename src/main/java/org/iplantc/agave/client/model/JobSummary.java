package org.iplantc.agave.client.model;

import java.util.Date;

public class JobSummary {
	/*
	 * The unique name of the application being run by this job. This must be a
	 * valid application that the calling user has permission to run.
	 */
	private String appId = null;
	/* The date the job ended in ISO 8601 format. */
	private Date endTime = null;
	/* The system id of the execution system. */
	private String executionSystem = null;
	/* The unique id of the job. */
	private Integer id = null;
	/* The name of the job. */
	private String name = null;
	/* The job owner. */
	private String owner = null;
	/* The date the job started in ISO 8601 format. */
	private Date startTime = null;
	/*
	 * The status of the job. Possible values are: PENDING, STAGING_INPUTS,
	 * CLEANING_UP, ARCHIVING, STAGING_JOB, FINISHED, KILLED, FAILED, STOPPED,
	 * RUNNING, PAUSED, QUEUED, SUBMITTING, STAGED, PROCESSING_INPUTS,
	 * ARCHIVING_FINISHED, ARCHIVING_FAILED
	 */
	private String status = null;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getExecutionSystem() {
		return executionSystem;
	}

	public void setExecutionSystem(String executionSystem) {
		this.executionSystem = executionSystem;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class JobSummary {\n");
		sb.append("  appId: ").append(appId).append("\n");
		sb.append("  endTime: ").append(endTime).append("\n");
		sb.append("  executionSystem: ").append(executionSystem).append("\n");
		sb.append("  id: ").append(id).append("\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("  owner: ").append(owner).append("\n");
		sb.append("  startTime: ").append(startTime).append("\n");
		sb.append("  status: ").append(status).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
