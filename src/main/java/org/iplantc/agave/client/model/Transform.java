package org.iplantc.agave.client.model;

import java.util.*;
import org.iplantc.agave.client.model.TransformEncoder;
import org.iplantc.agave.client.model.TransformDecoder;

public class Transform {
	/* The available decoders for this transform. */
	private List<TransformDecoder> decoders = new ArrayList<TransformDecoder>();
	/* Description of this transform. */
	private String description = null;
	/* The URL to find out more information. */
	private String descriptionurl = null;
	/* Whether this transform is enabled or not. */
	private Boolean enabled = null;
	/* The encoder to use to convert to this format. */
	private TransformEncoder encoder = null;
	/* The name of this transform. */
	private String name = null;
	/* The tags describing this transform. */
	private List<String> tags = new ArrayList<String>();

	public List<TransformDecoder> getDecoders() {
		return decoders;
	}

	public void setDecoders(List<TransformDecoder> decoders) {
		this.decoders = decoders;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescriptionurl() {
		return descriptionurl;
	}

	public void setDescriptionurl(String descriptionurl) {
		this.descriptionurl = descriptionurl;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public TransformEncoder getEncoder() {
		return encoder;
	}

	public void setEncoder(TransformEncoder encoder) {
		this.encoder = encoder;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Transform {\n");
		sb.append("  decoders: ").append(decoders).append("\n");
		sb.append("  description: ").append(description).append("\n");
		sb.append("  descriptionurl: ").append(descriptionurl).append("\n");
		sb.append("  enabled: ").append(enabled).append("\n");
		sb.append("  encoder: ").append(encoder).append("\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("  tags: ").append(tags).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
