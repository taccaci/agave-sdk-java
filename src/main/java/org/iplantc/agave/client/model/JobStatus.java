package org.iplantc.agave.client.model;

public class JobStatus {
	/* The unique id of the job. */
	private Integer id = null;
	/*
	 * The status of the job. Possible values are: PENDING, STAGING_INPUTS,
	 * CLEANING_UP, ARCHIVING, STAGING_JOB, FINISHED, KILLED, FAILED, STOPPED,
	 * RUNNING, PAUSED, QUEUED, SUBMITTING, STAGED, PROCESSING_INPUTS,
	 * ARCHIVING_FINISHED, ARCHIVING_FAILED
	 */
	private String status = null;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class JobStatus {\n");
		sb.append("  id: ").append(id).append("\n");
		sb.append("  status: ").append(status).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
