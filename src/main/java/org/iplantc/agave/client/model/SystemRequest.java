package org.iplantc.agave.client.model;

import java.util.*;
import org.iplantc.agave.client.model.BatchQueue;
import org.iplantc.agave.client.model.StorageConfig;
import org.iplantc.agave.client.model.LoginConfig;

public class SystemRequest {
	/* Verbose description of this system. */
	private String description = null;
	/* Environment variables to set upon login prior to job submission. */
	private String environment = null;
	/* The execution paradigm used to run jobs on this system. */
	private String executionType = null;
	// public enum executionTypeEnum { HPC, CONDOR, CLI, };
	/* Unique identifier for this system. */
	private String id = null;
	/*
	 * The login config defining how to connect to this system for job
	 * submission.
	 */
	private LoginConfig login = null;
	/*
	 * The maximum number of jobs that can be simultaneously run on the system
	 * across all queues.
	 */
	private Integer maxSystemJobs = null;
	/*
	 * The maximum number of jobs that can be simultaneously run on the system
	 * across all queues by a single user.
	 */
	private Integer maxSystemJobsPerUser = null;
	/* Common name for this system. */
	private String name = null;
	/* The execution paradigm used to run jobs on this system. */
	private List<BatchQueue> queues = new ArrayList<BatchQueue>();
	/* The type of scheduled used to run jobs. */
	private String scheduler = null;
	// public enum schedulerEnum { COBALT, CONDOR, FORK, LOADLEVELER, LSF, MOAB,
	// PBS, SGE, SLURM, TORQUE, UNKNOWN, };
	/*
	 * The scratch directory where job execution directories will be created at
	 * runtime. The workDir is used if this is not specified.
	 */
	private String scratchDir = null;
	/* The site associated with this system. */
	private String site = null;
	/* Script to be run after login and prior to execution. */
	private String startupScript = null;
	/* The status of this system. Systems must be in UP status to be used. */
	private String status = null;
	// public enum statusEnum { UP, DOWN, UNKNOWN, };
	/*
	 * The storage config defining how to connect to this system for data
	 * staging.
	 */
	private StorageConfig storage = null;
	/* The type of this system. */
	private String type = null;
	// public enum typeEnum { EXECUTION, STORAGE, };
	/*
	 * The work directory where job execution directories will be created at
	 * runtime. This is used if scratchDir is not specified. If neither are
	 * specified, the job directory will be created in the system homeDir.
	 */
	private String workDir = null;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getExecutionType() {
		return executionType;
	}

	public void setExecutionType(String executionType) {
		this.executionType = executionType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LoginConfig getLogin() {
		return login;
	}

	public void setLogin(LoginConfig login) {
		this.login = login;
	}

	public Integer getMaxSystemJobs() {
		return maxSystemJobs;
	}

	public void setMaxSystemJobs(Integer maxSystemJobs) {
		this.maxSystemJobs = maxSystemJobs;
	}

	public Integer getMaxSystemJobsPerUser() {
		return maxSystemJobsPerUser;
	}

	public void setMaxSystemJobsPerUser(Integer maxSystemJobsPerUser) {
		this.maxSystemJobsPerUser = maxSystemJobsPerUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<BatchQueue> getQueues() {
		return queues;
	}

	public void setQueues(List<BatchQueue> queues) {
		this.queues = queues;
	}

	public String getScheduler() {
		return scheduler;
	}

	public void setScheduler(String scheduler) {
		this.scheduler = scheduler;
	}

	public String getScratchDir() {
		return scratchDir;
	}

	public void setScratchDir(String scratchDir) {
		this.scratchDir = scratchDir;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getStartupScript() {
		return startupScript;
	}

	public void setStartupScript(String startupScript) {
		this.startupScript = startupScript;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public StorageConfig getStorage() {
		return storage;
	}

	public void setStorage(StorageConfig storage) {
		this.storage = storage;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getWorkDir() {
		return workDir;
	}

	public void setWorkDir(String workDir) {
		this.workDir = workDir;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class SystemRequest {\n");
		sb.append("  description: ").append(description).append("\n");
		sb.append("  environment: ").append(environment).append("\n");
		sb.append("  executionType: ").append(executionType).append("\n");
		sb.append("  id: ").append(id).append("\n");
		sb.append("  login: ").append(login).append("\n");
		sb.append("  maxSystemJobs: ").append(maxSystemJobs).append("\n");
		sb.append("  maxSystemJobsPerUser: ").append(maxSystemJobsPerUser)
				.append("\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("  queues: ").append(queues).append("\n");
		sb.append("  scheduler: ").append(scheduler).append("\n");
		sb.append("  scratchDir: ").append(scratchDir).append("\n");
		sb.append("  site: ").append(site).append("\n");
		sb.append("  startupScript: ").append(startupScript).append("\n");
		sb.append("  status: ").append(status).append("\n");
		sb.append("  storage: ").append(storage).append("\n");
		sb.append("  type: ").append(type).append("\n");
		sb.append("  workDir: ").append(workDir).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
