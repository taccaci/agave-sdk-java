package org.iplantc.agave.client.model;

import java.util.Date;

public class JobHistory {
	/* The date of the event. */
	private Date created = null;
	/* A brief description of the event details. */
	private String description = null;
	/* The status of the job after this event. */
	private String status = null;

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class JobHistory {\n");
		sb.append("  created: ").append(created).append("\n");
		sb.append("  description: ").append(description).append("\n");
		sb.append("  status: ").append(status).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
