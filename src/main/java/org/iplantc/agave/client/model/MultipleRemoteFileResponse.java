package org.iplantc.agave.client.model;

import java.util.*;
import org.iplantc.agave.client.model.RemoteFile;

public class MultipleRemoteFileResponse {
	/* success or failure */
	private String message = null;
	/* response body */
	private List<RemoteFile> result = new ArrayList<RemoteFile>();
	/* success or failure */
	private String status = null;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<RemoteFile> getResult() {
		return result;
	}

	public void setResult(List<RemoteFile> result) {
		this.result = result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class MultipleRemoteFileResponse {\n");
		sb.append("  message: ").append(message).append("\n");
		sb.append("  result: ").append(result).append("\n");
		sb.append("  status: ").append(status).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
