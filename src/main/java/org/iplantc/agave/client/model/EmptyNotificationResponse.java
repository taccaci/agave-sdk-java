package org.iplantc.agave.client.model;

import org.iplantc.agave.client.model.EmptyNotification;

public class EmptyNotificationResponse {
	/* success or failure */
	private String message = null;
	private EmptyNotification result = null;
	/* success or failure */
	private String status = null;
	/* API version number */
	private String version = null;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public EmptyNotification getResult() {
		return result;
	}

	public void setResult(EmptyNotification result) {
		this.result = result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class EmptyNotificationResponse {\n");
		sb.append("  message: ").append(message).append("\n");
		sb.append("  result: ").append(result).append("\n");
		sb.append("  status: ").append(status).append("\n");
		sb.append("  version: ").append(version).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
