package org.iplantc.agave.client.api;

import java.util.HashMap;
import java.util.Map;

import org.iplantc.agave.client.ApiException;
import org.iplantc.agave.client.ApiInvoker;
import org.iplantc.agave.client.Settings;
import org.iplantc.agave.client.model.AccessToken;
import org.iplantc.agave.client.model.EmptyRemoteFileResponse;
import org.iplantc.agave.client.model.FileHistoryResponse;
import org.iplantc.agave.client.model.FileOperationRequest;
import org.iplantc.agave.client.model.FilePermissionRequest;
import org.iplantc.agave.client.model.MultiplePermissionResponse;
import org.iplantc.agave.client.model.MultipleRemoteFileResponse;
import org.iplantc.agave.client.model.SingleRemoteFileResponse;

public class FilesApi extends AbstractApi {
	String basePath = Settings.API_BASE_URL;
	ApiInvoker apiInvoker = ApiInvoker.getInstance();

	public FilesApi(AccessToken token) {
		super(token);
		this.token = token;
	}

	public ApiInvoker getInvoker() {
		return apiInvoker;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getBasePath() {
		return basePath;
	}

	public void downloadFromDefaultSystem(String sourcefilePath)
			throws ApiException {
		// verify required params are set
		if (sourcefilePath == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/files/" + Settings.API_VERSION + "/media/{sourcefilePath}".replaceAll(
				"\\{format\\}", "json").replaceAll(
				"\\{" + "sourcefilePath" + "\\}",
				apiInvoker.escapeString(sourcefilePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return;
			} else {
				return;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return;
			} else {
				throw ex;
			}
		}
	}

	public SingleRemoteFileResponse importToDefaultSystem(
			String sourcefilePath, String fileType, String callbackURL,
			String fileName, String urlToIngest) throws ApiException {
		// verify required params are set
		if (sourcefilePath == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/files/" + Settings.API_VERSION + "/media/{sourcefilePath}".replaceAll(
				"\\{format\\}", "json").replaceAll(
				"\\{" + "sourcefilePath" + "\\}",
				apiInvoker.escapeString(sourcefilePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		formParams.put("fileType", fileType);
		formParams.put("callbackURL", callbackURL);
		formParams.put("fileName", fileName);
		formParams.put("urlToIngest", urlToIngest);
		String[] contentTypes = { "multipart/form-data" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleRemoteFileResponse) ApiInvoker.deserialize(
						response, "", SingleRemoteFileResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyRemoteFileResponse manageOnDefaultSystem(String sourcefilePath,
			FileOperationRequest body) throws ApiException {
		// verify required params are set
		if (sourcefilePath == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/files/" + Settings.API_VERSION + "/media/{sourcefilePath}".replaceAll(
				"\\{format\\}", "json").replaceAll(
				"\\{" + "sourcefilePath" + "\\}",
				apiInvoker.escapeString(sourcefilePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "PUT",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyRemoteFileResponse) ApiInvoker.deserialize(
						response, "", EmptyRemoteFileResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyRemoteFileResponse deleteFromDefaultSystem(String sourcefilePath)
			throws ApiException {
		// verify required params are set
		if (sourcefilePath == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/files/" + Settings.API_VERSION + "/media/{sourcefilePath}".replaceAll(
				"\\{format\\}", "json").replaceAll(
				"\\{" + "sourcefilePath" + "\\}",
				apiInvoker.escapeString(sourcefilePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyRemoteFileResponse) ApiInvoker.deserialize(
						response, "", EmptyRemoteFileResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public void download(String systemId, String filePath) throws ApiException {
		// verify required params are set
		if (systemId == null || filePath == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/files/" + Settings.API_VERSION + "/media/system/{systemId}/{filePath}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "systemId" + "\\}",
						apiInvoker.escapeString(systemId.toString()))
				.replaceAll("\\{" + "filePath" + "\\}",
						apiInvoker.escapeString(filePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return;
			} else {
				return;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return;
			} else {
				throw ex;
			}
		}
	}

	public SingleRemoteFileResponse importData(String systemId,
			String filePath, String fileType, String callbackURL,
			String fileName, String urlToIngest) throws ApiException {
		// verify required params are set
		if (systemId == null || filePath == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/files/" + Settings.API_VERSION + "/media/system/{systemId}/{filePath}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "systemId" + "\\}",
						apiInvoker.escapeString(systemId.toString()))
				.replaceAll("\\{" + "filePath" + "\\}",
						apiInvoker.escapeString(filePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		formParams.put("fileType", fileType);
		formParams.put("callbackURL", callbackURL);
		formParams.put("fileName", fileName);
		formParams.put("urlToIngest", urlToIngest);
		String[] contentTypes = { "multipart/form-data" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleRemoteFileResponse) ApiInvoker.deserialize(
						response, "", SingleRemoteFileResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyRemoteFileResponse manage(String systemId, String filePath,
			FileOperationRequest body) throws ApiException {
		// verify required params are set
		if (systemId == null || filePath == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/files/" + Settings.API_VERSION + "/media/system/{systemId}/{filePath}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "systemId" + "\\}",
						apiInvoker.escapeString(systemId.toString()))
				.replaceAll("\\{" + "filePath" + "\\}",
						apiInvoker.escapeString(filePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "PUT",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyRemoteFileResponse) ApiInvoker.deserialize(
						response, "", EmptyRemoteFileResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyRemoteFileResponse delete(String systemId, String filePath)
			throws ApiException {
		// verify required params are set
		if (systemId == null || filePath == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/files/" + Settings.API_VERSION + "/media/system/{systemId}/{filePath}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "systemId" + "\\}",
						apiInvoker.escapeString(systemId.toString()))
				.replaceAll("\\{" + "filePath" + "\\}",
						apiInvoker.escapeString(filePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyRemoteFileResponse) ApiInvoker.deserialize(
						response, "", EmptyRemoteFileResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleRemoteFileResponse listOnDefaultSystem(String filePath)
			throws ApiException {
		// verify required params are set
		if (filePath == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/files/" + Settings.API_VERSION + "/listings/{filePath}".replaceAll(
				"\\{format\\}", "json").replaceAll("\\{" + "filePath" + "\\}",
				apiInvoker.escapeString(filePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleRemoteFileResponse) ApiInvoker.deserialize(
						response, "", MultipleRemoteFileResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleRemoteFileResponse list(String systemId, String filePath)
			throws ApiException {
		// verify required params are set
		if (systemId == null || filePath == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/files/" + Settings.API_VERSION + "/listings/system/{systemId}/{filePath}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "systemId" + "\\}",
						apiInvoker.escapeString(systemId.toString()))
				.replaceAll("\\{" + "filePath" + "\\}",
						apiInvoker.escapeString(filePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleRemoteFileResponse) ApiInvoker.deserialize(
						response, "", MultipleRemoteFileResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public FileHistoryResponse getHistoryOnDefaultSystem(String filePath)
			throws ApiException {
		// verify required params are set
		if (filePath == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/files/" + Settings.API_VERSION + "/history/{filePath}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "filePath" + "\\}",
				apiInvoker.escapeString(filePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (FileHistoryResponse) ApiInvoker.deserialize(response,
						"", FileHistoryResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public FileHistoryResponse getHistory(String filePath) throws ApiException {
		// verify required params are set
		if (filePath == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/files/" + Settings.API_VERSION + "/history/system/{systemId}/{filePath}"
				.replaceAll("\\{format\\}", "json").replaceAll(
						"\\{" + "filePath" + "\\}",
						apiInvoker.escapeString(filePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (FileHistoryResponse) ApiInvoker.deserialize(response,
						"", FileHistoryResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultiplePermissionResponse listPermissionsOnDefaultSystem(
			String filePath) throws ApiException {
		// verify required params are set
		if (filePath == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/files/" + Settings.API_VERSION + "/pems/{filePath}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "filePath" + "\\}",
				apiInvoker.escapeString(filePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultiplePermissionResponse) ApiInvoker.deserialize(
						response, "", MultiplePermissionResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyRemoteFileResponse updatePermissionsOnDefaultSystem(
			String filePath, FilePermissionRequest body) throws ApiException {
		// verify required params are set
		if (filePath == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/files/" + Settings.API_VERSION + "/pems/{filePath}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "filePath" + "\\}",
				apiInvoker.escapeString(filePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyRemoteFileResponse) ApiInvoker.deserialize(
						response, "", EmptyRemoteFileResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultiplePermissionResponse listPermissions(String filePath)
			throws ApiException {
		// verify required params are set
		if (filePath == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/files/" + Settings.API_VERSION + "/pems/system/{systemId}/{filePath}".replaceAll(
				"\\{format\\}", "json").replaceAll("\\{" + "filePath" + "\\}",
				apiInvoker.escapeString(filePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultiplePermissionResponse) ApiInvoker.deserialize(
						response, "", MultiplePermissionResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyRemoteFileResponse updatePermissions(String filePath,
			FilePermissionRequest body) throws ApiException {
		// verify required params are set
		if (filePath == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/files/" + Settings.API_VERSION + "/pems/system/{systemId}/{filePath}".replaceAll(
				"\\{format\\}", "json").replaceAll("\\{" + "filePath" + "\\}",
				apiInvoker.escapeString(filePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyRemoteFileResponse) ApiInvoker.deserialize(
						response, "", EmptyRemoteFileResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyRemoteFileResponse deletePermissions(String filePath)
			throws ApiException {
		// verify required params are set
		if (filePath == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/files/" + Settings.API_VERSION + "/pems/system/{systemId}/{filePath}".replaceAll(
				"\\{format\\}", "json").replaceAll("\\{" + "filePath" + "\\}",
				apiInvoker.escapeString(filePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyRemoteFileResponse) ApiInvoker.deserialize(
						response, "", EmptyRemoteFileResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}
}
