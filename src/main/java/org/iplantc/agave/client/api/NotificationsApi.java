package org.iplantc.agave.client.api;

import java.util.HashMap;
import java.util.Map;

import org.iplantc.agave.client.ApiException;
import org.iplantc.agave.client.ApiInvoker;
import org.iplantc.agave.client.Settings;
import org.iplantc.agave.client.model.AccessToken;
import org.iplantc.agave.client.model.EmptyNotificationResponse;
import org.iplantc.agave.client.model.MultipleNotificationResponse;
import org.iplantc.agave.client.model.NotificationRequest;
import org.iplantc.agave.client.model.NotificationResponse;

public class NotificationsApi extends AbstractApi  {
	String basePath = Settings.API_BASE_URL;
	ApiInvoker apiInvoker = ApiInvoker.getInstance();

	public NotificationsApi(AccessToken token) {
		super(token);
		this.token = token;
	}

	public ApiInvoker getInvoker() {
		return apiInvoker;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getBasePath() {
		return basePath;
	}

	public NotificationResponse add(NotificationRequest body)
			throws ApiException {
		// verify required params are set
		if (body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/notifications/" + Settings.API_VERSION + "/".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (NotificationResponse) ApiInvoker.deserialize(response,
						"", NotificationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleNotificationResponse list(String associatedUuid)
			throws ApiException {
		// verify required params are set
		if (associatedUuid == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/notifications/" + Settings.API_VERSION + "/".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		if (!"null".equals(String.valueOf(associatedUuid)))
			queryParams.put("associatedUuid", String.valueOf(associatedUuid));
		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleNotificationResponse) ApiInvoker.deserialize(
						response, "", MultipleNotificationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public NotificationResponse get(String uuid) throws ApiException {
		// verify required params are set
		if (uuid == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/notifications/" + Settings.API_VERSION + "/{uuid}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "uuid" + "\\}",
				apiInvoker.escapeString(uuid.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (NotificationResponse) ApiInvoker.deserialize(response,
						"", NotificationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public NotificationResponse update(String uuid, NotificationRequest body)
			throws ApiException {
		// verify required params are set
		if (uuid == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/notifications/" + Settings.API_VERSION + "/{uuid}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "uuid" + "\\}",
				apiInvoker.escapeString(uuid.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (NotificationResponse) ApiInvoker.deserialize(response,
						"", NotificationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyNotificationResponse delete(String uuid) throws ApiException {
		// verify required params are set
		if (uuid == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/notifications/" + Settings.API_VERSION + "/{uuid}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "uuid" + "\\}",
				apiInvoker.escapeString(uuid.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyNotificationResponse) ApiInvoker.deserialize(
						response, "", EmptyNotificationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}
}
