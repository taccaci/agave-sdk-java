package org.iplantc.agave.client.api;

import java.util.HashMap;
import java.util.Map;

import org.iplantc.agave.client.ApiException;
import org.iplantc.agave.client.ApiInvoker;
import org.iplantc.agave.client.Settings;
import org.iplantc.agave.client.model.AccessToken;
import org.iplantc.agave.client.model.ApplicationFormResponse;
import org.iplantc.agave.client.model.ApplicationOperationRequest;
import org.iplantc.agave.client.model.ApplicationPermissionRequest;
import org.iplantc.agave.client.model.ApplicationPermissionResponse;
import org.iplantc.agave.client.model.ApplicationRequest;
import org.iplantc.agave.client.model.EmptyApplicationResponse;
import org.iplantc.agave.client.model.MultipleApplicationResponse;
import org.iplantc.agave.client.model.SingleApplicationResponse;

public class AppsApi extends AbstractApi {
	String basePath = Settings.API_BASE_URL;
	ApiInvoker apiInvoker = ApiInvoker.getInstance();

	public AppsApi(AccessToken token) {
		super(token);
	}

	public ApiInvoker getInvoker() {
		return apiInvoker;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getBasePath() {
		return basePath;
	}

	public MultipleApplicationResponse list(boolean b, boolean c) throws ApiException
	{
		// create path and map variables
		String path = "/apps/" + Settings.API_VERSION + "/".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		if (!"null".equals(String.valueOf(b)))
			queryParams.put("publicOnly", String.valueOf(b));
		if (!"null".equals(String.valueOf(c)))
			queryParams.put("privateOnly", String.valueOf(c));
		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleApplicationResponse) ApiInvoker.deserialize(
						response, "", MultipleApplicationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleApplicationResponse add(ApplicationRequest body)
			throws ApiException {
		// verify required params are set
		if (body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/apps/" + Settings.API_VERSION + "/".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleApplicationResponse) ApiInvoker.deserialize(
						response, "", SingleApplicationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleApplicationResponse get(String appId) throws ApiException {
		// verify required params are set
		if (appId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/apps/" + Settings.API_VERSION + "/{appId}".replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "appId" + "\\}",
						apiInvoker.escapeString(appId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleApplicationResponse) ApiInvoker.deserialize(
						response, "", SingleApplicationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleApplicationResponse update(String appId,
			ApplicationRequest body) throws ApiException {
		// verify required params are set
		if (appId == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/apps/" + Settings.API_VERSION + "/{appId}".replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "appId" + "\\}",
						apiInvoker.escapeString(appId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleApplicationResponse) ApiInvoker.deserialize(
						response, "", SingleApplicationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleApplicationResponse manage(String appId,
			ApplicationOperationRequest body) throws ApiException {
		// verify required params are set
		if (appId == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/apps/" + Settings.API_VERSION + "/{appId}".replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "appId" + "\\}",
						apiInvoker.escapeString(appId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "PUT",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleApplicationResponse) ApiInvoker.deserialize(
						response, "", SingleApplicationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyApplicationResponse delete(String appId) throws ApiException {
		// verify required params are set
		if (appId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/apps/" + Settings.API_VERSION + "/{appId}".replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "appId" + "\\}",
						apiInvoker.escapeString(appId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyApplicationResponse) ApiInvoker.deserialize(
						response, "", EmptyApplicationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public ApplicationPermissionResponse listPermissions(String appId)
			throws ApiException {
		// verify required params are set
		if (appId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/apps/" + Settings.API_VERSION + "/{appId}/pems"
				.replaceAll("\\{format\\}", "json").replaceAll(
						"\\{" + "appId" + "\\}",
						apiInvoker.escapeString(appId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (ApplicationPermissionResponse) ApiInvoker.deserialize(
						response, "", ApplicationPermissionResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyApplicationResponse updateApplicationPermissions(String appId,
			ApplicationPermissionRequest body) throws ApiException {
		// verify required params are set
		if (appId == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/apps/" + Settings.API_VERSION + "/{appId}/pems"
				.replaceAll("\\{format\\}", "json").replaceAll(
						"\\{" + "appId" + "\\}",
						apiInvoker.escapeString(appId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyApplicationResponse) ApiInvoker.deserialize(
						response, "", EmptyApplicationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyApplicationResponse deletePermissions(String appId)
			throws ApiException {
		// verify required params are set
		if (appId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/apps/" + Settings.API_VERSION + "/{appId}/pems"
				.replaceAll("\\{format\\}", "json").replaceAll(
						"\\{" + "appId" + "\\}",
						apiInvoker.escapeString(appId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyApplicationResponse) ApiInvoker.deserialize(
						response, "", EmptyApplicationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public ApplicationPermissionResponse listPermissionsForUser(String appId,
			String username) throws ApiException {
		// verify required params are set
		if (appId == null || username == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/apps/" + Settings.API_VERSION + "/{appId}/pems/{username}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "appId" + "\\}",
						apiInvoker.escapeString(appId.toString()))
				.replaceAll("\\{" + "username" + "\\}",
						apiInvoker.escapeString(username.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (ApplicationPermissionResponse) ApiInvoker.deserialize(
						response, "", ApplicationPermissionResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyApplicationResponse updatePermissionsForUser(String appId,
			String username, ApplicationPermissionRequest body)
			throws ApiException {
		// verify required params are set
		if (appId == null || username == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/apps/" + Settings.API_VERSION + "/{appId}/pems/{username}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "appId" + "\\}",
						apiInvoker.escapeString(appId.toString()))
				.replaceAll("\\{" + "username" + "\\}",
						apiInvoker.escapeString(username.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyApplicationResponse) ApiInvoker.deserialize(
						response, "", EmptyApplicationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyApplicationResponse deletePermissionsForUser(String appId,
			String username) throws ApiException {
		// verify required params are set
		if (appId == null || username == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/apps/" + Settings.API_VERSION + "/{appId}/pems/{username}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "appId" + "\\}",
						apiInvoker.escapeString(appId.toString()))
				.replaceAll("\\{" + "username" + "\\}",
						apiInvoker.escapeString(username.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyApplicationResponse) ApiInvoker.deserialize(
						response, "", EmptyApplicationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleApplicationResponse listByName(String name,
			String publicOnly, String privateOnly) throws ApiException {
		// verify required params are set
		if (name == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/apps/" + Settings.API_VERSION + "/name/{name}".replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "name" + "\\}",
						apiInvoker.escapeString(name.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		if (!"null".equals(String.valueOf(publicOnly)))
			queryParams.put("publicOnly", String.valueOf(publicOnly));
		if (!"null".equals(String.valueOf(privateOnly)))
			queryParams.put("privateOnly", String.valueOf(privateOnly));
		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleApplicationResponse) ApiInvoker.deserialize(
						response, "", MultipleApplicationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleApplicationResponse listByTag(String tag, String publicOnly,
			String privateOnly) throws ApiException {
		// verify required params are set
		if (tag == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/apps/" + Settings.API_VERSION + "/tag/{tag}".replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "tag" + "\\}",
						apiInvoker.escapeString(tag.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		if (!"null".equals(String.valueOf(publicOnly)))
			queryParams.put("publicOnly", String.valueOf(publicOnly));
		if (!"null".equals(String.valueOf(privateOnly)))
			queryParams.put("privateOnly", String.valueOf(privateOnly));
		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleApplicationResponse) ApiInvoker.deserialize(
						response, "", MultipleApplicationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleApplicationResponse listByOntologyTerm(String term,
			String publicOnly, String privateOnly) throws ApiException {
		// verify required params are set
		if (term == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/apps/" + Settings.API_VERSION + "/ontology/{term}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "term" + "\\}",
				apiInvoker.escapeString(term.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		if (!"null".equals(String.valueOf(publicOnly)))
			queryParams.put("publicOnly", String.valueOf(publicOnly));
		if (!"null".equals(String.valueOf(privateOnly)))
			queryParams.put("privateOnly", String.valueOf(privateOnly));
		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleApplicationResponse) ApiInvoker.deserialize(
						response, "", MultipleApplicationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public ApplicationFormResponse getJobSubmissionForm() throws ApiException {
		// create path and map variables
		String path = "/apps/" + Settings.API_VERSION + "/{uniqueName}/form".replaceAll("\\{format\\}",
				"json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (ApplicationFormResponse) ApiInvoker.deserialize(
						response, "", ApplicationFormResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleApplicationResponse listBySystemId(String systemId,
			String publicOnly, String privateOnly) throws ApiException {
		// verify required params are set
		if (systemId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/apps/" + Settings.API_VERSION + "/system/{systemId}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "systemId" + "\\}",
				apiInvoker.escapeString(systemId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		if (!"null".equals(String.valueOf(publicOnly)))
			queryParams.put("publicOnly", String.valueOf(publicOnly));
		if (!"null".equals(String.valueOf(privateOnly)))
			queryParams.put("privateOnly", String.valueOf(privateOnly));
		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleApplicationResponse) ApiInvoker.deserialize(
						response, "", MultipleApplicationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}
}
