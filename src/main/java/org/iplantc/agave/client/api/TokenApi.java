package org.iplantc.agave.client.api;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.iplantc.agave.client.ApiException;
import org.iplantc.agave.client.ApiInvoker;
import org.iplantc.agave.client.JsonUtil;
import org.iplantc.agave.client.Settings;
import org.iplantc.agave.client.model.AccessToken;
import org.iplantc.agave.client.model.TokenResponse;
import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.JsonNode;
import com.sun.jersey.core.util.Base64;

public class TokenApi {
	String basePath = Settings.API_BASE_URL;
	ApiInvoker apiInvoker = ApiInvoker.getInstance();

	public ApiInvoker getInvoker() {
		return apiInvoker;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getBasePath() {
		return basePath;
	}

	public TokenResponse createAuthToken(String username, String password, String clientKey, String clientSecret)
	throws ApiException
	{
		if (StringUtils.isEmpty(username)) {
			throw new ApiException(400, "missing required username");
		}
		
		if (StringUtils.isEmpty(password)) {
			throw new ApiException(400, "missing required username");
		}
		
		if (StringUtils.isEmpty(clientKey)) {
			throw new ApiException(400, "missing required clientKey");
		}
		
		if (StringUtils.isEmpty(clientSecret)) {
			throw new ApiException(400, "missing required clientSecret");
		}
		
		// create path and map variables
		String path = "/token";

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("BASIC", clientKey + ":" + clientSecret);
		
		Map<String, String> formParams = new HashMap<String, String>();
		formParams.put("grant_type", "client_credentials");
		formParams.put("username", username);
		formParams.put("password", password);
		formParams.put("scope", "PRODUCTION");
		

		String[] contentTypes = { "application/x-www-form-urlencoded" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/x-www-form-urlencoded";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, "", headerParams, formParams, contentType);
			if (response != null) {	
				return deserialize(clientKey, clientSecret, response);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}
	
	public TokenResponse refreshAuthToken(String clientKey, String clientSecret, String refreshToken)
	throws ApiException
	{
		if (StringUtils.isEmpty(clientKey)) {
			throw new ApiException(400, "missing required clientKey");
		}
		
		if (StringUtils.isEmpty(clientSecret)) {
			throw new ApiException(400, "missing required clientSecret");
		}
		
		if (StringUtils.isEmpty(refreshToken)) {
			throw new ApiException(400, "missing required refreshToken");
		}
		
		// create path and map variables
		String path = "/token";

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("BASIC", clientKey + ":" + clientSecret);
		
		Map<String, String> formParams = new HashMap<String, String>();
		formParams.put("grant_type", "refresh_token");
		formParams.put("refresh_token", refreshToken);
		formParams.put("scope", "PRODUCTION");
		

		String[] contentTypes = { "application/x-www-form-urlencoded" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/x-www-form-urlencoded";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, "", headerParams, formParams, contentType);
			if (response != null) {
				return deserialize(clientKey, clientSecret, response);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}
	
	private TokenResponse deserialize(String clientKey, String clientSecret, String response) 
	throws ApiException
	{
		TokenResponse tokenResponse = new TokenResponse();
		try 
		{
			JsonNode json = JsonUtil.getJsonMapper().readTree(response);
			if (json.has("access_token")) {
				tokenResponse.setStatus("success");
				tokenResponse.setMessage(null);
			
				int lifetime = 14400;
				int expiresIn = json.get("expires_in").asInt();
				DateTime created = new DateTime();
				created = created.minusSeconds(lifetime - expiresIn);
				
				AccessToken token = new AccessToken();
				token.setAccessToken(json.get("access_token").asText());
				token.setClientSecret(clientSecret);
				token.setClientKey(clientKey);
				token.setCreatedAt(created);
				token.setExpiresAt(new DateTime().plusSeconds(expiresIn));
				token.setRefreshToken(json.get("refresh_token").asText());
				
				tokenResponse.setResult(token);
			}
			else 
			{
				tokenResponse.setStatus("error");
				tokenResponse.setMessage(null);
				tokenResponse.setResult(null);
			}
			return tokenResponse;
		} catch (Exception e) {
			throw new ApiException(500, e.getMessage());
		}
	}
}
