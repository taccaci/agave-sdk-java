package org.iplantc.agave.client.api;

import java.util.HashMap;
import java.util.Map;

import org.iplantc.agave.client.ApiException;
import org.iplantc.agave.client.ApiInvoker;
import org.iplantc.agave.client.Settings;
import org.iplantc.agave.client.model.AccessToken;
import org.iplantc.agave.client.model.EmptyMonitorResponse;
import org.iplantc.agave.client.model.MonitorCheckResponse;
import org.iplantc.agave.client.model.MonitorRequest;
import org.iplantc.agave.client.model.MonitorResponse;
import org.iplantc.agave.client.model.MultipleMonitorCheckResponse;
import org.iplantc.agave.client.model.MultipleMonitorResponse;

public class MonitorsApi extends AbstractApi  {
	String basePath = Settings.API_BASE_URL;
	ApiInvoker apiInvoker = ApiInvoker.getInstance();

	public MonitorsApi(AccessToken token) {
		super(token);
		this.token = token;
	}

	public ApiInvoker getInvoker() {
		return apiInvoker;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getBasePath() {
		return basePath;
	}

	public MultipleMonitorResponse list(String target, String active)
			throws ApiException {
		// create path and map variables
		String path = "/monitors/" + Settings.API_VERSION + "/".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		if (!"null".equals(String.valueOf(target)))
			queryParams.put("target", String.valueOf(target));
		if (!"null".equals(String.valueOf(active)))
			queryParams.put("active", String.valueOf(active));
		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleMonitorResponse) ApiInvoker.deserialize(
						response, "", MultipleMonitorResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MonitorResponse add(MonitorRequest body) throws ApiException {
		// verify required params are set
		if (body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/monitors/" + Settings.API_VERSION + "/".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (MonitorResponse) ApiInvoker.deserialize(response, "",
						MonitorResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MonitorResponse get(String monitorId) throws ApiException {
		// verify required params are set
		if (monitorId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/monitors/" + Settings.API_VERSION + "/{monitorId}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "monitorId" + "\\}",
				apiInvoker.escapeString(monitorId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MonitorResponse) ApiInvoker.deserialize(response, "",
						MonitorResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MonitorResponse update(MonitorRequest body) throws ApiException {
		// verify required params are set
		if (body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/monitors/" + Settings.API_VERSION + "/{monitorId}".replaceAll("\\{format\\}",
				"json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (MonitorResponse) ApiInvoker.deserialize(response, "",
						MonitorResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyMonitorResponse delete(String monitorId) throws ApiException {
		// verify required params are set
		if (monitorId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/monitors/" + Settings.API_VERSION + "/{monitorId}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "monitorId" + "\\}",
				apiInvoker.escapeString(monitorId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyMonitorResponse) ApiInvoker.deserialize(response,
						"", EmptyMonitorResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleMonitorCheckResponse listChecks(String monitorId,
			String startDate, String endDate, String result, Integer limit,
			Integer offset) throws ApiException {
		// verify required params are set
		if (monitorId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/monitors/" + Settings.API_VERSION + "/{monitorId}/checks".replaceAll(
				"\\{format\\}", "json").replaceAll("\\{" + "monitorId" + "\\}",
				apiInvoker.escapeString(monitorId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		if (!"null".equals(String.valueOf(startDate)))
			queryParams.put("startDate", String.valueOf(startDate));
		if (!"null".equals(String.valueOf(endDate)))
			queryParams.put("endDate", String.valueOf(endDate));
		if (!"null".equals(String.valueOf(result)))
			queryParams.put("result", String.valueOf(result));
		if (!"null".equals(String.valueOf(limit)))
			queryParams.put("limit", String.valueOf(limit));
		if (!"null".equals(String.valueOf(offset)))
			queryParams.put("offset", String.valueOf(offset));
		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleMonitorCheckResponse) ApiInvoker.deserialize(
						response, "", MultipleMonitorCheckResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MonitorCheckResponse runCheck(String monitorId) throws ApiException {
		// verify required params are set
		if (monitorId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/monitors/" + Settings.API_VERSION + "/{monitorId}/checks".replaceAll(
				"\\{format\\}", "json").replaceAll("\\{" + "monitorId" + "\\}",
				apiInvoker.escapeString(monitorId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MonitorCheckResponse) ApiInvoker.deserialize(response,
						"", MonitorCheckResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MonitorCheckResponse getCheck(String monitorId, String checkId)
			throws ApiException {
		// verify required params are set
		if (monitorId == null || checkId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/monitors/" + Settings.API_VERSION + "/{monitorId}/checks/{checkId}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "monitorId" + "\\}",
						apiInvoker.escapeString(monitorId.toString()))
				.replaceAll("\\{" + "checkId" + "\\}",
						apiInvoker.escapeString(checkId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MonitorCheckResponse) ApiInvoker.deserialize(response,
						"", MonitorCheckResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}
}
