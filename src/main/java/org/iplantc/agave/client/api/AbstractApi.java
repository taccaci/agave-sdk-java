package org.iplantc.agave.client.api;

import org.iplantc.agave.client.model.AccessToken;

public class AbstractApi {
	
	protected AccessToken token;
	
	public AbstractApi(AccessToken token) {
		this.token = token;
	}
	
	public String checkAndGetAccessTokenString() {
		if (token != null && token.getExpiresAt().isAfterNow()) {
			return token.getAccessToken();
		} else {
			return null;
		}
	}
}
