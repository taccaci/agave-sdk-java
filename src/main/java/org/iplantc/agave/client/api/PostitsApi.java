package org.iplantc.agave.client.api;

import java.util.HashMap;
import java.util.Map;

import org.iplantc.agave.client.ApiException;
import org.iplantc.agave.client.ApiInvoker;
import org.iplantc.agave.client.Settings;
import org.iplantc.agave.client.model.AccessToken;
import org.iplantc.agave.client.model.MultiplePostItResponse;
import org.iplantc.agave.client.model.PostIt;
import org.iplantc.agave.client.model.PostItRequest;
import org.iplantc.agave.client.model.SinglePostItResponse;

public class PostitsApi extends AbstractApi  {
	String basePath = Settings.API_BASE_URL;
	ApiInvoker apiInvoker = ApiInvoker.getInstance();

	public PostitsApi(AccessToken token) {
		super(token);
		this.token = token;
	}

	public ApiInvoker getInvoker() {
		return apiInvoker;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getBasePath() {
		return basePath;
	}

	public MultiplePostItResponse list() throws ApiException {
		// create path and map variables
		String path = "/postits/" + Settings.API_VERSION + "/".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultiplePostItResponse) ApiInvoker.deserialize(
						response, "", MultiplePostItResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public PostIt create(PostItRequest body) throws ApiException {
		// verify required params are set
		if (body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/postits/" + Settings.API_VERSION + "/".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (PostIt) ApiInvoker.deserialize(response, "",
						PostIt.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SinglePostItResponse delete(String nonce) throws ApiException {
		// verify required params are set
		if (nonce == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/postits/" + Settings.API_VERSION + "/{nonce}".replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "nonce" + "\\}",
						apiInvoker.escapeString(nonce.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (SinglePostItResponse) ApiInvoker.deserialize(response,
						"", SinglePostItResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}
}
