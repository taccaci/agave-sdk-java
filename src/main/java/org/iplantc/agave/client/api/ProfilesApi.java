package org.iplantc.agave.client.api;

import java.util.HashMap;
import java.util.Map;

import org.iplantc.agave.client.ApiException;
import org.iplantc.agave.client.ApiInvoker;
import org.iplantc.agave.client.Settings;
import org.iplantc.agave.client.model.AccessToken;
import org.iplantc.agave.client.model.InternalUserRequest;
import org.iplantc.agave.client.model.MultipleInternalUserResponse;
import org.iplantc.agave.client.model.MultipleProfileResponse;
import org.iplantc.agave.client.model.SingleInternalUserResponse;

public class ProfilesApi extends AbstractApi  {
	String basePath = Settings.API_BASE_URL;
	ApiInvoker apiInvoker = ApiInvoker.getInstance();
	
	public ProfilesApi(AccessToken token) {
		super(token);
		this.token = token;
	}

	public ApiInvoker getInvoker() {
		return apiInvoker;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getBasePath() {
		return basePath;
	}

	public MultipleProfileResponse get() throws ApiException {
		// create path and map variables
		String path = "/profiles/" + Settings.API_VERSION + "/".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());

		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleProfileResponse) ApiInvoker.deserialize(
						response, "", MultipleProfileResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleProfileResponse listByUsername(String username)
			throws ApiException {
		// verify required params are set
		if (username == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/profiles/" + Settings.API_VERSION + "/{username}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "username" + "\\}",
				apiInvoker.escapeString(username.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleProfileResponse) ApiInvoker.deserialize(
						response, "", MultipleProfileResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleProfileResponse search(String term, String value)
			throws ApiException {
		// verify required params are set
		if (term == null || value == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/profiles/" + Settings.API_VERSION + "/search/{term}/{value}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "term" + "\\}",
						apiInvoker.escapeString(term.toString()))
				.replaceAll("\\{" + "value" + "\\}",
						apiInvoker.escapeString(value.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleProfileResponse) ApiInvoker.deserialize(
						response, "", MultipleProfileResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleInternalUserResponse listInternalUsers(String apiUsername)
			throws ApiException {
		// verify required params are set
		if (apiUsername == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/profiles/" + Settings.API_VERSION + "/{apiUsername}/users".replaceAll(
				"\\{format\\}", "json").replaceAll(
				"\\{" + "apiUsername" + "\\}",
				apiInvoker.escapeString(apiUsername.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleInternalUserResponse) ApiInvoker.deserialize(
						response, "", MultipleInternalUserResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleInternalUserResponse deleteInternalUsers(String apiUsername)
			throws ApiException {
		// verify required params are set
		if (apiUsername == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/profiles/" + Settings.API_VERSION + "/{apiUsername}/users".replaceAll(
				"\\{format\\}", "json").replaceAll(
				"\\{" + "apiUsername" + "\\}",
				apiInvoker.escapeString(apiUsername.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleInternalUserResponse) ApiInvoker.deserialize(
						response, "", SingleInternalUserResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleInternalUserResponse addInternalUser(String apiUsername,
			InternalUserRequest body) throws ApiException {
		// verify required params are set
		if (apiUsername == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/profiles/" + Settings.API_VERSION + "/{apiUsername}/users".replaceAll(
				"\\{format\\}", "json").replaceAll(
				"\\{" + "apiUsername" + "\\}",
				apiInvoker.escapeString(apiUsername.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleInternalUserResponse) ApiInvoker.deserialize(
						response, "", SingleInternalUserResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleInternalUserResponse getInternalUser(String apiUsername,
			String internalUsername) throws ApiException {
		// verify required params are set
		if (apiUsername == null || internalUsername == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/profiles/" + Settings.API_VERSION + "/{apiUsername}/users/{internalUsername}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "apiUsername" + "\\}",
						apiInvoker.escapeString(apiUsername.toString()))
				.replaceAll("\\{" + "internalUsername" + "\\}",
						apiInvoker.escapeString(internalUsername.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleInternalUserResponse) ApiInvoker.deserialize(
						response, "", SingleInternalUserResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleInternalUserResponse deleteInternalUser(String apiUsername,
			String internalUsername) throws ApiException {
		// verify required params are set
		if (apiUsername == null || internalUsername == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/profiles/" + Settings.API_VERSION + "/{apiUsername}/users/{internalUsername}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "apiUsername" + "\\}",
						apiInvoker.escapeString(apiUsername.toString()))
				.replaceAll("\\{" + "internalUsername" + "\\}",
						apiInvoker.escapeString(internalUsername.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleInternalUserResponse) ApiInvoker.deserialize(
						response, "", SingleInternalUserResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleInternalUserResponse updateInternalUser(String apiUsername,
			String internalUsername, InternalUserRequest body)
			throws ApiException {
		// verify required params are set
		if (apiUsername == null || internalUsername == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/profiles/" + Settings.API_VERSION + "/{apiUsername}/users/{internalUsername}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "apiUsername" + "\\}",
						apiInvoker.escapeString(apiUsername.toString()))
				.replaceAll("\\{" + "internalUsername" + "\\}",
						apiInvoker.escapeString(internalUsername.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleInternalUserResponse) ApiInvoker.deserialize(
						response, "", SingleInternalUserResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleProfileResponse searchInternalUsers(String username,
			String term, String value) throws ApiException {
		// verify required params are set
		if (username == null || term == null || value == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/profiles/" + Settings.API_VERSION + "/{username}/users/search/{term}/{value}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "username" + "\\}",
						apiInvoker.escapeString(username.toString()))
				.replaceAll("\\{" + "term" + "\\}",
						apiInvoker.escapeString(term.toString()))
				.replaceAll("\\{" + "value" + "\\}",
						apiInvoker.escapeString(value.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleProfileResponse) ApiInvoker.deserialize(
						response, "", MultipleProfileResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}
}
