package org.iplantc.agave.client.api;

import java.util.HashMap;
import java.util.Map;

import org.iplantc.agave.client.ApiException;
import org.iplantc.agave.client.ApiInvoker;
import org.iplantc.agave.client.Settings;
import org.iplantc.agave.client.model.AccessToken;
import org.iplantc.agave.client.model.MultipleTransformResponse;
import org.iplantc.agave.client.model.TransformRequest;

public class TransformsApi extends AbstractApi  {
	String basePath = Settings.API_BASE_URL;
	ApiInvoker apiInvoker = ApiInvoker.getInstance();

	public TransformsApi(AccessToken token) {
		super(token);
		this.token = token;
	}

	public ApiInvoker getInvoker() {
		return apiInvoker;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getBasePath() {
		return basePath;
	}

	public MultipleTransformResponse list() throws ApiException {
		// create path and map variables
		String path = "/transforms/" + Settings.API_VERSION + "/".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleTransformResponse) ApiInvoker.deserialize(
						response, "", MultipleTransformResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleTransformResponse get(String transformId)
			throws ApiException {
		// verify required params are set
		if (transformId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/transforms/" + Settings.API_VERSION + "/{transformId}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "transformId" + "\\}",
				apiInvoker.escapeString(transformId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleTransformResponse) ApiInvoker.deserialize(
						response, "", MultipleTransformResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleTransformResponse transformAndStage(String transformId,
			String owner, String filePath, TransformRequest body)
			throws ApiException {
		// verify required params are set
		if (transformId == null || owner == null || filePath == null
				|| body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/transforms/" + Settings.API_VERSION + "/{transformId}/async/{owner}/{filePath}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "transformId" + "\\}",
						apiInvoker.escapeString(transformId.toString()))
				.replaceAll("\\{" + "owner" + "\\}",
						apiInvoker.escapeString(owner.toString()))
				.replaceAll("\\{" + "filePath" + "\\}",
						apiInvoker.escapeString(filePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleTransformResponse) ApiInvoker.deserialize(
						response, "", MultipleTransformResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public void transformAndDownload(String transformId, String owner,
			String filePath, TransformRequest body) throws ApiException {
		// verify required params are set
		if (transformId == null || owner == null || filePath == null
				|| body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/transforms/" + Settings.API_VERSION + "/{transformId}/sync/{owner}/{filePath}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "transformId" + "\\}",
						apiInvoker.escapeString(transformId.toString()))
				.replaceAll("\\{" + "owner" + "\\}",
						apiInvoker.escapeString(owner.toString()))
				.replaceAll("\\{" + "filePath" + "\\}",
						apiInvoker.escapeString(filePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return;
			} else {
				return;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return;
			} else {
				throw ex;
			}
		}
	}

	public MultipleTransformResponse listByTag(String tag) throws ApiException {
		// verify required params are set
		if (tag == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/transforms/" + Settings.API_VERSION + "/tags/{tag}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "tag" + "\\}",
				apiInvoker.escapeString(tag.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleTransformResponse) ApiInvoker.deserialize(
						response, "", MultipleTransformResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}
}
