package org.iplantc.agave.client.api;

import java.util.HashMap;
import java.util.Map;

import org.iplantc.agave.client.ApiException;
import org.iplantc.agave.client.ApiInvoker;
import org.iplantc.agave.client.Settings;
import org.iplantc.agave.client.model.AccessToken;
import org.iplantc.agave.client.model.EmptySystemResponse;
import org.iplantc.agave.client.model.MultipleSystemResponse;
import org.iplantc.agave.client.model.MultipleSystemRoleResponse;
import org.iplantc.agave.client.model.SingleSystemResponse;
import org.iplantc.agave.client.model.SystemCredentialsResponse;
import org.iplantc.agave.client.model.SystemOperationRequest;
import org.iplantc.agave.client.model.SystemRequest;
import org.iplantc.agave.client.model.SystemRole;
import org.iplantc.agave.client.model.UserCredential;

public class SystemsApi extends AbstractApi  {
	String basePath = Settings.API_BASE_URL;
	ApiInvoker apiInvoker = ApiInvoker.getInstance();

	public SystemsApi(AccessToken token) {
		super(token);
		this.token = token;
	}

	public ApiInvoker getInvoker() {
		return apiInvoker;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getBasePath() {
		return basePath;
	}

	public MultipleSystemResponse list(String type, Boolean defaultOnly,
			Boolean publicOnly, Boolean privateOnly) throws ApiException {
		// verify required params are set
		if (defaultOnly == null || publicOnly == null || privateOnly == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "defaultOnly" + "\\}",
						apiInvoker.escapeString(defaultOnly.toString()))
				.replaceAll("\\{" + "publicOnly" + "\\}",
						apiInvoker.escapeString(publicOnly.toString()))
				.replaceAll("\\{" + "privateOnly" + "\\}",
						apiInvoker.escapeString(privateOnly.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		if (!"null".equals(String.valueOf(type)))
			queryParams.put("type", String.valueOf(type));
		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleSystemResponse) ApiInvoker.deserialize(
						response, "", MultipleSystemResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleSystemResponse add(SystemRequest body) throws ApiException {
		// verify required params are set
		if (body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleSystemResponse) ApiInvoker.deserialize(response,
						"", SingleSystemResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleSystemResponse get(String systemId) throws ApiException {
		// verify required params are set
		if (systemId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "systemId" + "\\}",
				apiInvoker.escapeString(systemId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleSystemResponse) ApiInvoker.deserialize(response,
						"", SingleSystemResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleSystemResponse update(String systemId, SystemRequest body)
			throws ApiException {
		// verify required params are set
		if (systemId == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "systemId" + "\\}",
				apiInvoker.escapeString(systemId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleSystemResponse) ApiInvoker.deserialize(response,
						"", SingleSystemResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptySystemResponse manage(String systemId,
			SystemOperationRequest body) throws ApiException {
		// verify required params are set
		if (systemId == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "systemId" + "\\}",
				apiInvoker.escapeString(systemId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "PUT",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptySystemResponse) ApiInvoker.deserialize(response,
						"", EmptySystemResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptySystemResponse delete(String systemId) throws ApiException {
		// verify required params are set
		if (systemId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "systemId" + "\\}",
				apiInvoker.escapeString(systemId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptySystemResponse) ApiInvoker.deserialize(response,
						"", EmptySystemResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleSystemRoleResponse listRoles(String systemId)
			throws ApiException {
		// verify required params are set
		if (systemId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}/roles".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "systemId" + "\\}",
				apiInvoker.escapeString(systemId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleSystemRoleResponse) ApiInvoker.deserialize(
						response, "", MultipleSystemRoleResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptySystemResponse updateRole(String systemId, SystemRole body)
			throws ApiException {
		// verify required params are set
		if (systemId == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}/roles".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "systemId" + "\\}",
				apiInvoker.escapeString(systemId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptySystemResponse) ApiInvoker.deserialize(response,
						"", EmptySystemResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptySystemResponse deleteRoles(String systemId) throws ApiException {
		// verify required params are set
		if (systemId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}/roles".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "systemId" + "\\}",
				apiInvoker.escapeString(systemId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptySystemResponse) ApiInvoker.deserialize(response,
						"", EmptySystemResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleSystemRoleResponse listRolesForUser(String systemId,
			String username) throws ApiException {
		// verify required params are set
		if (systemId == null || username == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}/roles/{username}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "systemId" + "\\}",
						apiInvoker.escapeString(systemId.toString()))
				.replaceAll("\\{" + "username" + "\\}",
						apiInvoker.escapeString(username.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleSystemRoleResponse) ApiInvoker.deserialize(
						response, "", MultipleSystemRoleResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptySystemResponse updateRoleForUser(String systemId,
			String username, SystemRole body) throws ApiException {
		// verify required params are set
		if (systemId == null || username == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}/roles/{username}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "systemId" + "\\}",
						apiInvoker.escapeString(systemId.toString()))
				.replaceAll("\\{" + "username" + "\\}",
						apiInvoker.escapeString(username.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptySystemResponse) ApiInvoker.deserialize(response,
						"", EmptySystemResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptySystemResponse deleteRoleForUser(String systemId,
			String username) throws ApiException {
		// verify required params are set
		if (systemId == null || username == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}/roles/{username}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "systemId" + "\\}",
						apiInvoker.escapeString(systemId.toString()))
				.replaceAll("\\{" + "username" + "\\}",
						apiInvoker.escapeString(username.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptySystemResponse) ApiInvoker.deserialize(response,
						"", EmptySystemResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SystemCredentialsResponse listCredentials(String systemId)
			throws ApiException {
		// verify required params are set
		if (systemId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}/credentials".replaceAll(
				"\\{format\\}", "json").replaceAll("\\{" + "systemId" + "\\}",
				apiInvoker.escapeString(systemId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (SystemCredentialsResponse) ApiInvoker.deserialize(
						response, "", SystemCredentialsResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptySystemResponse updateCredentials(String systemId,
			UserCredential body) throws ApiException {
		// verify required params are set
		if (systemId == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}/credentials".replaceAll(
				"\\{format\\}", "json").replaceAll("\\{" + "systemId" + "\\}",
				apiInvoker.escapeString(systemId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptySystemResponse) ApiInvoker.deserialize(response,
						"", EmptySystemResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptySystemResponse deleteCredentials(String systemId)
			throws ApiException {
		// verify required params are set
		if (systemId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}/credentials".replaceAll(
				"\\{format\\}", "json").replaceAll("\\{" + "systemId" + "\\}",
				apiInvoker.escapeString(systemId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptySystemResponse) ApiInvoker.deserialize(response,
						"", EmptySystemResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SystemCredentialsResponse listCredentialsForInternalUser(
			String systemId, String internalUsername) throws ApiException {
		// verify required params are set
		if (systemId == null || internalUsername == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}/credentials/{internalUsername}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "systemId" + "\\}",
						apiInvoker.escapeString(systemId.toString()))
				.replaceAll("\\{" + "internalUsername" + "\\}",
						apiInvoker.escapeString(internalUsername.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (SystemCredentialsResponse) ApiInvoker.deserialize(
						response, "", SystemCredentialsResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptySystemResponse updateCredentialsForInternalUser(
			String systemId, String internalUsername, UserCredential body)
			throws ApiException {
		// verify required params are set
		if (systemId == null || internalUsername == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}/credentials/{internalUsername}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "systemId" + "\\}",
						apiInvoker.escapeString(systemId.toString()))
				.replaceAll("\\{" + "internalUsername" + "\\}",
						apiInvoker.escapeString(internalUsername.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptySystemResponse) ApiInvoker.deserialize(response,
						"", EmptySystemResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptySystemResponse deleteCredentialsForInternalUser(
			String systemId, String internalUsername) throws ApiException {
		// verify required params are set
		if (systemId == null || internalUsername == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}/credentials/{internalUsername}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "systemId" + "\\}",
						apiInvoker.escapeString(systemId.toString()))
				.replaceAll("\\{" + "internalUsername" + "\\}",
						apiInvoker.escapeString(internalUsername.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptySystemResponse) ApiInvoker.deserialize(response,
						"", EmptySystemResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SystemCredentialsResponse listCredentialsForInternalUserByType(
			String systemId, String internalUsername, String credentialType)
			throws ApiException {
		// verify required params are set
		if (systemId == null || internalUsername == null
				|| credentialType == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}/credentials/{internalUsername}/{credentialType}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "systemId" + "\\}",
						apiInvoker.escapeString(systemId.toString()))
				.replaceAll("\\{" + "internalUsername" + "\\}",
						apiInvoker.escapeString(internalUsername.toString()))
				.replaceAll("\\{" + "credentialType" + "\\}",
						apiInvoker.escapeString(credentialType.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (SystemCredentialsResponse) ApiInvoker.deserialize(
						response, "", SystemCredentialsResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptySystemResponse updateCredentialsForInternalUserByType(
			String systemId, String internalUsername, String credentialType,
			UserCredential body) throws ApiException {
		// verify required params are set
		if (systemId == null || internalUsername == null
				|| credentialType == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}/credentials/{internalUsername}/{credentialType}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "systemId" + "\\}",
						apiInvoker.escapeString(systemId.toString()))
				.replaceAll("\\{" + "internalUsername" + "\\}",
						apiInvoker.escapeString(internalUsername.toString()))
				.replaceAll("\\{" + "credentialType" + "\\}",
						apiInvoker.escapeString(credentialType.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptySystemResponse) ApiInvoker.deserialize(response,
						"", EmptySystemResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptySystemResponse deleteCredentialsForInternalUserByType(
			String systemId, String internalUsername, String credentialType)
			throws ApiException {
		// verify required params are set
		if (systemId == null || internalUsername == null
				|| credentialType == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/systems/" + Settings.API_VERSION + "/{systemId}/credentials/{internalUsername}/{credentialType}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "systemId" + "\\}",
						apiInvoker.escapeString(systemId.toString()))
				.replaceAll("\\{" + "internalUsername" + "\\}",
						apiInvoker.escapeString(internalUsername.toString()))
				.replaceAll("\\{" + "credentialType" + "\\}",
						apiInvoker.escapeString(credentialType.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptySystemResponse) ApiInvoker.deserialize(response,
						"", EmptySystemResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}
}
