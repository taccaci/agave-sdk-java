package org.iplantc.agave.client.api;

import java.util.HashMap;
import java.util.Map;

import org.iplantc.agave.client.ApiException;
import org.iplantc.agave.client.ApiInvoker;
import org.iplantc.agave.client.Settings;
import org.iplantc.agave.client.model.AccessToken;
import org.iplantc.agave.client.model.EmptyMetadataResponse;
import org.iplantc.agave.client.model.MetadataPermissionRequest;
import org.iplantc.agave.client.model.MetadataPermissionResponse;
import org.iplantc.agave.client.model.MetadataRequest;
import org.iplantc.agave.client.model.MetadataResponse;
import org.iplantc.agave.client.model.MetadataSchemaPermissionResponse;
import org.iplantc.agave.client.model.MetadataSchemaResponse;
import org.iplantc.agave.client.model.MultipleMetadataPermissionResponse;
import org.iplantc.agave.client.model.MultipleMetadataResponse;
import org.iplantc.agave.client.model.MultipleMetadataSchemaPermissionResponse;

public class MetaApi extends AbstractApi {
	String basePath = Settings.API_BASE_URL;
	ApiInvoker apiInvoker = ApiInvoker.getInstance();

	public MetaApi(AccessToken token) {
		super(token);
		this.token = token;
	}

	public ApiInvoker getInvoker() {
		return apiInvoker;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getBasePath() {
		return basePath;
	}

	public MultipleMetadataResponse listMetadata(String q) throws ApiException {
		// verify required params are set
		if (q == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/data".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		if (!"null".equals(String.valueOf(q)))
			queryParams.put("q", String.valueOf(q));
		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleMetadataResponse) ApiInvoker.deserialize(
						response, "", MultipleMetadataResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MetadataResponse addMetadata(MetadataRequest body)
			throws ApiException {
		// verify required params are set
		if (body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/data".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (MetadataResponse) ApiInvoker.deserialize(response, "",
						MetadataResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MetadataResponse getMetadata(String uuid) throws ApiException {
		// verify required params are set
		if (uuid == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/data/{uuid}".replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "uuid" + "\\}",
						apiInvoker.escapeString(uuid.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MetadataResponse) ApiInvoker.deserialize(response, "",
						MetadataResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MetadataResponse updateMetadata(String uuid, MetadataRequest body)
			throws ApiException {
		// verify required params are set
		if (uuid == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/data/{uuid}".replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "uuid" + "\\}",
						apiInvoker.escapeString(uuid.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (MetadataResponse) ApiInvoker.deserialize(response, "",
						MetadataResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyMetadataResponse deleteMetadata(String uuid)
			throws ApiException {
		// verify required params are set
		if (uuid == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/data/{uuid}".replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "uuid" + "\\}",
						apiInvoker.escapeString(uuid.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyMetadataResponse) ApiInvoker.deserialize(response,
						"", EmptyMetadataResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MetadataSchemaResponse searchSchema(String uuid) throws ApiException {
		// verify required params are set
		if (uuid == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/schemas".replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "uuid" + "\\}",
						apiInvoker.escapeString(uuid.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MetadataSchemaResponse) ApiInvoker.deserialize(
						response, "", MetadataSchemaResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MetadataSchemaResponse addSchema(String body) throws ApiException {
		// verify required params are set
		if (body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/schemas".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (MetadataSchemaResponse) ApiInvoker.deserialize(
						response, "", MetadataSchemaResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MetadataSchemaResponse getSchema(String uuid) throws ApiException {
		// verify required params are set
		if (uuid == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/schemas/{uuid}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "uuid" + "\\}",
				apiInvoker.escapeString(uuid.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MetadataSchemaResponse) ApiInvoker.deserialize(
						response, "", MetadataSchemaResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MetadataSchemaResponse updateSchema(String uuid, String body)
			throws ApiException {
		// verify required params are set
		if (uuid == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/schemas/{uuid}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "uuid" + "\\}",
				apiInvoker.escapeString(uuid.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (MetadataSchemaResponse) ApiInvoker.deserialize(
						response, "", MetadataSchemaResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyMetadataResponse deleteSchema(String uuid) throws ApiException {
		// verify required params are set
		if (uuid == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/schemas/{uuid}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "uuid" + "\\}",
				apiInvoker.escapeString(uuid.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyMetadataResponse) ApiInvoker.deserialize(response,
						"", EmptyMetadataResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleMetadataPermissionResponse listMetadataPermissions(
			String uuid) throws ApiException {
		// verify required params are set
		if (uuid == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/data/{uuid}/pems".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "uuid" + "\\}",
				apiInvoker.escapeString(uuid.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleMetadataPermissionResponse) ApiInvoker
						.deserialize(response, "",
								MultipleMetadataPermissionResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MetadataPermissionResponse updateMetadataPermissions(String uuid,
			MetadataPermissionRequest body) throws ApiException {
		// verify required params are set
		if (uuid == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/data/{uuid}/pems".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "uuid" + "\\}",
				apiInvoker.escapeString(uuid.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (MetadataPermissionResponse) ApiInvoker.deserialize(
						response, "", MetadataPermissionResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyMetadataResponse deleteMetadataPermission(String uuid)
			throws ApiException {
		// verify required params are set
		if (uuid == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/data/{uuid}/pems".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "uuid" + "\\}",
				apiInvoker.escapeString(uuid.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyMetadataResponse) ApiInvoker.deserialize(response,
						"", EmptyMetadataResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MetadataPermissionResponse listMetadataPermissionsForUser(
			String uuid, String username) throws ApiException {
		// verify required params are set
		if (uuid == null || username == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/data/{uuid}/pems/{username}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "uuid" + "\\}",
						apiInvoker.escapeString(uuid.toString()))
				.replaceAll("\\{" + "username" + "\\}",
						apiInvoker.escapeString(username.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MetadataPermissionResponse) ApiInvoker.deserialize(
						response, "", MetadataPermissionResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MetadataPermissionResponse updateMetadataPermissionsForUser(
			String uuid, String username, MetadataPermissionRequest body)
			throws ApiException {
		// verify required params are set
		if (uuid == null || username == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/data/{uuid}/pems/{username}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "uuid" + "\\}",
						apiInvoker.escapeString(uuid.toString()))
				.replaceAll("\\{" + "username" + "\\}",
						apiInvoker.escapeString(username.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (MetadataPermissionResponse) ApiInvoker.deserialize(
						response, "", MetadataPermissionResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyMetadataResponse deleteMetadataPermissionsForUser(String uuid,
			String username) throws ApiException {
		// verify required params are set
		if (uuid == null || username == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/data/{uuid}/pems/{username}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "uuid" + "\\}",
						apiInvoker.escapeString(uuid.toString()))
				.replaceAll("\\{" + "username" + "\\}",
						apiInvoker.escapeString(username.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyMetadataResponse) ApiInvoker.deserialize(response,
						"", EmptyMetadataResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleMetadataSchemaPermissionResponse listSchemaPermissions(
			String uuid) throws ApiException {
		// verify required params are set
		if (uuid == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/schemas/{uuid}/pems".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "uuid" + "\\}",
				apiInvoker.escapeString(uuid.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleMetadataSchemaPermissionResponse) ApiInvoker
						.deserialize(response, "",
								MultipleMetadataSchemaPermissionResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MetadataSchemaPermissionResponse updateSchemaPermissions(
			String uuid, MetadataPermissionRequest body) throws ApiException {
		// verify required params are set
		if (uuid == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/schemas/{uuid}/pems".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "uuid" + "\\}",
				apiInvoker.escapeString(uuid.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (MetadataSchemaPermissionResponse) ApiInvoker
						.deserialize(response, "",
								MetadataSchemaPermissionResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyMetadataResponse deleteSchemaPermissions(String uuid)
			throws ApiException {
		// verify required params are set
		if (uuid == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/schemas/{uuid}/pems".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "uuid" + "\\}",
				apiInvoker.escapeString(uuid.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyMetadataResponse) ApiInvoker.deserialize(response,
						"", EmptyMetadataResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MetadataSchemaPermissionResponse listSchemaPermissionsForUser(
			String uuid, String username) throws ApiException {
		// verify required params are set
		if (uuid == null || username == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/schemas/{uuid}/pems/{username}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "uuid" + "\\}",
						apiInvoker.escapeString(uuid.toString()))
				.replaceAll("\\{" + "username" + "\\}",
						apiInvoker.escapeString(username.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MetadataSchemaPermissionResponse) ApiInvoker
						.deserialize(response, "",
								MetadataSchemaPermissionResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MetadataSchemaPermissionResponse updateSchemaPermissionsForUser(
			String uuid, String username, MetadataPermissionRequest body)
			throws ApiException {
		// verify required params are set
		if (uuid == null || username == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/schemas/{uuid}/pems/{username}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "uuid" + "\\}",
						apiInvoker.escapeString(uuid.toString()))
				.replaceAll("\\{" + "username" + "\\}",
						apiInvoker.escapeString(username.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (MetadataSchemaPermissionResponse) ApiInvoker
						.deserialize(response, "",
								MetadataSchemaPermissionResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyMetadataResponse deleteSchemaPermissionsForUser(String uuid,
			String username) throws ApiException {
		// verify required params are set
		if (uuid == null || username == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/meta/" + Settings.API_VERSION + "/schemas/{uuid}/pems/{username}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "uuid" + "\\}",
						apiInvoker.escapeString(uuid.toString()))
				.replaceAll("\\{" + "username" + "\\}",
						apiInvoker.escapeString(username.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyMetadataResponse) ApiInvoker.deserialize(response,
						"", EmptyMetadataResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}
}
