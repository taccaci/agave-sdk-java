package org.iplantc.agave.client.api;

import org.iplantc.agave.client.ApiException;
import org.iplantc.agave.client.ApiInvoker;
import org.iplantc.agave.client.Settings;
import org.iplantc.agave.client.model.SingleSubscriptionResponse;
import org.iplantc.agave.client.model.MultipleClientResponse;
import org.iplantc.agave.client.model.SingleClientResponse;
import org.iplantc.agave.client.model.ClientSubscriptionRequest;
import org.iplantc.agave.client.model.EmptyClientResponse;
import org.iplantc.agave.client.model.MultipleSubscriptionResponse;
import org.iplantc.agave.client.model.ClientRequest;

import com.sun.jersey.core.util.Base64;

import java.util.*;

public class ClientsApi {
	String basePath = "https://agave.iplantc.org";
	ApiInvoker apiInvoker = ApiInvoker.getInstance();

	public ApiInvoker getInvoker() {
		return apiInvoker;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getBasePath() {
		return basePath;
	}

	public MultipleClientResponse list(String username, String password) throws ApiException {
		// create path and map variables
		String path = "/clients/" + Settings.API_VERSION + "/".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> formParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", new String(Base64.encode(username + ":" + password)));

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleClientResponse) ApiInvoker.deserialize(
						response, "", MultipleClientResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleClientResponse create(String username, String password, ClientRequest body) throws ApiException {
		// verify required params are set
		if (body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/clients/" + Settings.API_VERSION + "/".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> formParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", new String(Base64.encode(username + ":" + password)));
		
		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleClientResponse) ApiInvoker.deserialize(response,
						"", SingleClientResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleClientResponse getClientByName(String username, String password, String clientName)
			throws ApiException {
		// verify required params are set
		if (clientName == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/clients/" + Settings.API_VERSION + "/{clientName}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "clientName" + "\\}",
				apiInvoker.escapeString(clientName.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> formParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", new String(Base64.encode(username + ":" + password)));
		
		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleClientResponse) ApiInvoker.deserialize(
						response, "", SingleClientResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyClientResponse delete(String username, String password, String clientName) throws ApiException {
		// verify required params are set
		if (clientName == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/clients/" + Settings.API_VERSION + "/{clientName}".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "clientName" + "\\}",
				apiInvoker.escapeString(clientName.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> formParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", new String(Base64.encode(username + ":" + password)));
		
		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyClientResponse) ApiInvoker.deserialize(response,
						"", EmptyClientResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleSubscriptionResponse listSubscriptionsForClient(
			String username, String password, String clientName) 
	throws ApiException 
	{
		// verify required params are set
		if (clientName == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/clients/" + Settings.API_VERSION + "/{clientName}/subscriptions".replaceAll(
				"\\{format\\}", "json").replaceAll(
				"\\{" + "clientName" + "\\}",
				apiInvoker.escapeString(clientName.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> formParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", new String(Base64.encode(username + ":" + password)));
		
		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleSubscriptionResponse) ApiInvoker.deserialize(
						response, "", MultipleSubscriptionResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleSubscriptionResponse addSubscriptionForClient(
			String username, String password, String clientName, ClientSubscriptionRequest body)
    throws ApiException 
	{
		// verify required params are set
		if (clientName == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/clients/" + Settings.API_VERSION + "/{clientName}/subscriptions".replaceAll(
				"\\{format\\}", "json").replaceAll(
				"\\{" + "clientName" + "\\}",
				apiInvoker.escapeString(clientName.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> formParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", new String(Base64.encode(username + ":" + password)));
		
		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleSubscriptionResponse) ApiInvoker.deserialize(
						response, "", SingleSubscriptionResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyClientResponse deleteSubscriptionsForClient(String username, String password, String clientName)
	throws ApiException 
	{
		// verify required params are set
		if (clientName == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/clients/" + Settings.API_VERSION + "/{clientName}/subscriptions".replaceAll(
				"\\{format\\}", "json").replaceAll(
				"\\{" + "clientName" + "\\}",
				apiInvoker.escapeString(clientName.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> formParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", new String(Base64.encode(username + ":" + password)));
		
		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyClientResponse) ApiInvoker.deserialize(response,
						"", EmptyClientResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}
}
