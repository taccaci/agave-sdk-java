package org.iplantc.agave.client.api;

import java.util.HashMap;
import java.util.Map;

import org.iplantc.agave.client.ApiException;
import org.iplantc.agave.client.ApiInvoker;
import org.iplantc.agave.client.Settings;
import org.iplantc.agave.client.model.AccessToken;
import org.iplantc.agave.client.model.EmptyApplicationResponse;
import org.iplantc.agave.client.model.EmptyJobResponse;
import org.iplantc.agave.client.model.JobHistoryResponse;
import org.iplantc.agave.client.model.JobOperationRequest;
import org.iplantc.agave.client.model.JobOutputFileDownload;
import org.iplantc.agave.client.model.JobPermissionRequest;
import org.iplantc.agave.client.model.JobPermissionResponse;
import org.iplantc.agave.client.model.JobRequest;
import org.iplantc.agave.client.model.JobStatusResponse;
import org.iplantc.agave.client.model.MultipleJobResponse;
import org.iplantc.agave.client.model.MultipleRemoteFileResponse;
import org.iplantc.agave.client.model.SingleJobResponse;

public class JobsApi extends AbstractApi {
	String basePath = Settings.API_BASE_URL;
	ApiInvoker apiInvoker = ApiInvoker.getInstance();

	public JobsApi(AccessToken token) {
		super(token);
		this.token = token;
	}

	public ApiInvoker getInvoker() {
		return apiInvoker;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getBasePath() {
		return basePath;
	}

	public MultipleJobResponse list() throws ApiException {
		// create path and map variables
		String path = "/jobs/" + Settings.API_VERSION + "/".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleJobResponse) ApiInvoker.deserialize(response,
						"", MultipleJobResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleJobResponse submit(JobRequest body) throws ApiException {
		// verify required params are set
		if (body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/jobs/" + Settings.API_VERSION + "/".replaceAll("\\{format\\}", "json");

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleJobResponse) ApiInvoker.deserialize(response, "",
						SingleJobResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleJobResponse get(Integer jobId) throws ApiException {
		// verify required params are set
		if (jobId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/jobs/" + Settings.API_VERSION + "/{jobId}".replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "jobId" + "\\}",
						apiInvoker.escapeString(jobId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleJobResponse) ApiInvoker.deserialize(response, "",
						SingleJobResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public SingleJobResponse manage(String jobId, JobOperationRequest body)
			throws ApiException {
		// verify required params are set
		if (jobId == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/jobs/" + Settings.API_VERSION + "/{jobId}".replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "jobId" + "\\}",
						apiInvoker.escapeString(jobId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (SingleJobResponse) ApiInvoker.deserialize(response, "",
						SingleJobResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyJobResponse delete(String jobId) throws ApiException {
		// verify required params are set
		if (jobId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/jobs/" + Settings.API_VERSION + "/{jobId}".replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "jobId" + "\\}",
						apiInvoker.escapeString(jobId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyJobResponse) ApiInvoker.deserialize(response, "",
						EmptyJobResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public JobHistoryResponse getHistory(String jobId) throws ApiException {
		// verify required params are set
		if (jobId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/jobs/" + Settings.API_VERSION + "/{jobId}/history".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "jobId" + "\\}",
				apiInvoker.escapeString(jobId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (JobHistoryResponse) ApiInvoker.deserialize(response,
						"", JobHistoryResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public JobPermissionResponse listPermissions(String jobId)
			throws ApiException {
		// verify required params are set
		if (jobId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/jobs/" + Settings.API_VERSION + "/{jobId}/pems"
				.replaceAll("\\{format\\}", "json").replaceAll(
						"\\{" + "jobId" + "\\}",
						apiInvoker.escapeString(jobId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (JobPermissionResponse) ApiInvoker.deserialize(response,
						"", JobPermissionResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyJobResponse updatePermissions(String jobId,
			JobPermissionRequest body) throws ApiException {
		// verify required params are set
		if (jobId == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/jobs/" + Settings.API_VERSION + "/{jobId}/pems"
				.replaceAll("\\{format\\}", "json").replaceAll(
						"\\{" + "jobId" + "\\}",
						apiInvoker.escapeString(jobId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyJobResponse) ApiInvoker.deserialize(response, "",
						EmptyJobResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyJobResponse deletePermissions(String jobId) throws ApiException {
		// verify required params are set
		if (jobId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/jobs/" + Settings.API_VERSION + "/{jobId}/pems"
				.replaceAll("\\{format\\}", "json").replaceAll(
						"\\{" + "jobId" + "\\}",
						apiInvoker.escapeString(jobId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyJobResponse) ApiInvoker.deserialize(response, "",
						EmptyJobResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public JobPermissionResponse listPermissionsForUser(String jobId,
			String username) throws ApiException {
		// verify required params are set
		if (jobId == null || username == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/jobs/" + Settings.API_VERSION + "/{jobId}/pems/{username}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "jobId" + "\\}",
						apiInvoker.escapeString(jobId.toString()))
				.replaceAll("\\{" + "username" + "\\}",
						apiInvoker.escapeString(username.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (JobPermissionResponse) ApiInvoker.deserialize(response,
						"", JobPermissionResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyJobResponse updatePermissionsForUser(String jobId,
			String username, JobPermissionRequest body) throws ApiException {
		// verify required params are set
		if (jobId == null || username == null || body == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/jobs/" + Settings.API_VERSION + "/{jobId}/pems/{username}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "jobId" + "\\}",
						apiInvoker.escapeString(jobId.toString()))
				.replaceAll("\\{" + "username" + "\\}",
						apiInvoker.escapeString(username.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "POST",
					queryParams, body, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyJobResponse) ApiInvoker.deserialize(response, "",
						EmptyJobResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public EmptyApplicationResponse deletePermissionsForUser(String uniqueName,
			String username) throws ApiException {
		// verify required params are set
		if (uniqueName == null || username == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/jobs/" + Settings.API_VERSION + "/{jobId}/pems/{username}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "uniqueName" + "\\}",
						apiInvoker.escapeString(uniqueName.toString()))
				.replaceAll("\\{" + "username" + "\\}",
						apiInvoker.escapeString(username.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "DELETE",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (EmptyApplicationResponse) ApiInvoker.deserialize(
						response, "", EmptyApplicationResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public JobStatusResponse getStatus(String jobId) throws ApiException {
		// verify required params are set
		if (jobId == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/jobs/" + Settings.API_VERSION + "/{jobId}/status".replaceAll("\\{format\\}",
				"json").replaceAll("\\{" + "jobId" + "\\}",
				apiInvoker.escapeString(jobId.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (JobStatusResponse) ApiInvoker.deserialize(response, "",
						JobStatusResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleRemoteFileResponse listOutputs(String jobId, String filePath)
			throws ApiException {
		// verify required params are set
		if (jobId == null || filePath == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/jobs/" + Settings.API_VERSION + "/{jobId}/ouputs/listings/{filePath}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "jobId" + "\\}",
						apiInvoker.escapeString(jobId.toString()))
				.replaceAll("\\{" + "filePath" + "\\}",
						apiInvoker.escapeString(filePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleRemoteFileResponse) ApiInvoker.deserialize(
						response, "", MultipleRemoteFileResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public JobOutputFileDownload downloadOutput(String jobId, String filePath)
			throws ApiException {
		// verify required params are set
		if (jobId == null || filePath == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/jobs/" + Settings.API_VERSION + "/{jobId}/ouputs/media/{filePath}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "jobId" + "\\}",
						apiInvoker.escapeString(jobId.toString()))
				.replaceAll("\\{" + "filePath" + "\\}",
						apiInvoker.escapeString(filePath.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (JobOutputFileDownload) ApiInvoker.deserialize(response,
						"", JobOutputFileDownload.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	public MultipleJobResponse search(String attribute, String value)
			throws ApiException {
		// verify required params are set
		if (attribute == null || value == null) {
			throw new ApiException(400, "missing required params");
		}
		// create path and map variables
		String path = "/jobs/" + Settings.API_VERSION + "/{attribute}/{value}"
				.replaceAll("\\{format\\}", "json")
				.replaceAll("\\{" + "attribute" + "\\}",
						apiInvoker.escapeString(attribute.toString()))
				.replaceAll("\\{" + "value" + "\\}",
						apiInvoker.escapeString(value.toString()));

		// query params
		Map<String, String> queryParams = new HashMap<String, String>();
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Authorization", "Bearer " + checkAndGetAccessTokenString());
		Map<String, String> formParams = new HashMap<String, String>();

		String[] contentTypes = { "application/json" };

		String contentType = contentTypes.length > 0 ? contentTypes[0]
				: "application/json";

		try {
			String response = apiInvoker.invokeAPI(basePath, path, "GET",
					queryParams, null, headerParams, formParams, contentType);
			if (response != null) {
				return (MultipleJobResponse) ApiInvoker.deserialize(response,
						"", MultipleJobResponse.class);
			} else {
				return null;
			}
		} catch (ApiException ex) {
			if (ex.getCode() == 404) {
				return null;
			} else {
				throw ex;
			}
		}
	}
}
