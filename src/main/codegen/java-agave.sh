#!/bin/sh

SCRIPT="$0"
SCALA_RUNNER_VERSION=$(scala ./bin/Version.scala)

while [ -h "$SCRIPT" ] ; do
  ls=`ls -ld "$SCRIPT"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    SCRIPT="$link"
  else
    SCRIPT=`dirname "$SCRIPT"`/"$link"
  fi
done

if [ ! -d "${APP_DIR}" ]; then
  APP_DIR=`dirname "$SCRIPT"`/..
  APP_DIR=`cd "${APP_DIR}"; pwd`
fi

cd $APP_DIR


# if you've executed sbt assembly previously it will use that instead.
export JAVA_OPTS="${JAVA_OPTS} -XX:MaxPermSize=256M -Xmx1024M -DfileMap=../agave/agave-apidocs/apidocs-api/target/classes/resources/index.html -DloggerPath=conf/log4j.properties"
ags="$@ samples/client/agave-sdk/agave-sdk-java/JavaAgaveCodegen.scala  http://localhost/v2/docs/resources special-key"

if [ -f $APP_DIR/target/scala-$SCALA_RUNNER_VERSION/swagger-codegen.jar ]; then
  scala -cp target/scala-$SCALA_RUNNER_VERSION/swagger-codegen.jar $ags
else
  echo "Please set scalaVersion := \"$SCALA_RUNNER_VERSION\" in build.sbt and run ./sbt assembly"
fi

perl -pi -e 's/datetime/DateTime/g' samples/client/agave-sdk/agave-sdk-java/src/main/java//org/iplantc/agave/client/model/*.java
perl -pi -e 's/Date-time/DateTime/g' samples/client/agave-sdk/agave-sdk-java/src/main/java//org/iplantc/agave/client/model/*.java
perl -pi -e 's/org.iplantc.agave.client.model.Datetime/org.joda.time.DateTime/g' samples/client/agave-sdk/agave-sdk-java/src/main/java//org/iplantc/agave/client/model/*.java
perl -pi -e 's/org.iplantc.agave.client.model.DateTime/org.joda.time.DateTime/g' samples/client/agave-sdk/agave-sdk-java/src/main/java//org/iplantc/agave/client/model/*.java

echo 'package org.iplantc.agave.client.api;' > samples/client/agave-sdk/agave-sdk-java/src/main/java//org/iplantc/agave/client/api/JobsApi.java2
echo '' >> samples/client/agave-sdk/agave-sdk-java/src/main/java//org/iplantc/agave/client/api/JobsApi.java2
echo 'import org.iplantc.agave.client.model.JobInputs;' >> samples/client/agave-sdk/agave-sdk-java/src/main/java//org/iplantc/agave/client/api/JobsApi.java2
echo 'import org.iplantc.agave.client.model.JobParameters;' >> samples/client/agave-sdk/agave-sdk-java/src/main/java//org/iplantc/agave/client/api/JobsApi.java2

tail -n +3 samples/client/agave-sdk/agave-sdk-java/src/main/java//org/iplantc/agave/client/api/JobsApi.java >> samples/client/agave-sdk/agave-sdk-java/src/main/java//org/iplantc/agave/client/api/JobsApi.java2
mv samples/client/agave-sdk/agave-sdk-java/src/main/java//org/iplantc/agave/client/api/JobsApi.java2 samples/client/agave-sdk/agave-sdk-java/src/main/java//org/iplantc/agave/client/api/JobsApi.java 

